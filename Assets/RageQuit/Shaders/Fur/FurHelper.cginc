﻿#pragma target 3.0

#include "Lighting.cginc"
#include "UnityCG.cginc"

struct v2f
{
    float4 pos: SV_POSITION;
    half4 uv: TEXCOORD0;
    float3 worldNormal: TEXCOORD1;
    float3 worldPos: TEXCOORD2;

	half3 tspace0 : TEXCOORD3;
	half3 tspace1 : TEXCOORD4;
	half3 tspace2 : TEXCOORD5;
};

fixed4 _Color;
fixed4 _Specular;
half _Shininess;

sampler2D _MainTex;
half4 _MainTex_ST;
sampler2D _SpecTex;
half4 _SpecTex_ST;
sampler2D _FurMask;
half4 _FurMask_ST;
sampler2D _FurTex;
half4 _FurTex_ST;
sampler2D _BumpMap;

fixed _FurLength;

v2f vert_surface(appdata_base v, float4 tangent : TANGENT)
{
    v2f o;
    o.pos = UnityObjectToClipPos(v.vertex);
    o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
    o.worldNormal = UnityObjectToWorldNormal(v.normal);
    o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

	half3 wTangent = UnityObjectToWorldDir(tangent.xyz);
	half tangentSign = tangent.w * unity_WorldTransformParams.w;
	half3 wBitangent = cross(o.worldNormal, wTangent) * tangentSign;

	o.tspace0 = half3(wTangent.x, wBitangent.x, o.worldNormal.x);
	o.tspace1 = half3(wTangent.y, wBitangent.y, o.worldNormal.y);
	o.tspace2 = half3(wTangent.z, wBitangent.z, o.worldNormal.z);

    return o;
}

v2f vert_base(appdata_base v, float4 tangent : TANGENT)
{
    v2f o;
    float3 P = v.vertex.xyz + v.normal * _FurLength * FURSTEP;
    o.pos = UnityObjectToClipPos(float4(P, 1.0));
    o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
	o.uv.zw = TRANSFORM_TEX(v.texcoord, _FurMask);
    o.uv.zw = TRANSFORM_TEX(v.texcoord, _FurTex);
    o.worldNormal = UnityObjectToWorldNormal(v.normal);
    o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

	half3 wTangent = UnityObjectToWorldDir(tangent.xyz);
	half tangentSign = tangent.w * unity_WorldTransformParams.w;
	half3 wBitangent = cross(o.worldNormal, wTangent) * tangentSign;

	o.tspace0 = half3(wTangent.x, wBitangent.x, o.worldNormal.x);
	o.tspace1 = half3(wTangent.y, wBitangent.y, o.worldNormal.y);
	o.tspace2 = half3(wTangent.z, wBitangent.z, o.worldNormal.z);

    return o;
}

fixed4 frag_surface(v2f i): SV_Target
{
	half3 tnormal = UnpackNormal(tex2D(_BumpMap, i.uv));

	half3 worldNormal;

	worldNormal.x = dot(i.tspace0, tnormal);
	worldNormal.y = dot(i.tspace1, tnormal);
	worldNormal.z = dot(i.tspace2, tnormal);

    fixed3 worldLight = normalize(_WorldSpaceLightPos0.xyz);
    fixed3 worldView = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xyz);
    fixed3 worldHalf = normalize(worldView + worldLight);
    
    fixed3 albedo = tex2D(_MainTex, i.uv.xy).rgb * _Color;
	fixed4 spec = tex2D(_SpecTex, i.uv.xy).rgba;
    fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz * albedo;
    fixed3 diffuse = _LightColor0.rgb * albedo * saturate(dot(worldNormal, worldLight));
    fixed3 specular = _LightColor0.rgb * spec.a * pow(saturate(dot(worldNormal, worldHalf)), _Shininess);

    fixed3 color = ambient + diffuse + specular;
    
    return fixed4(color, 1.0);
}

fixed4 frag_base(v2f i): SV_Target
{
	half3 tnormal = UnpackNormal(tex2D(_BumpMap, i.uv));

	half3 worldNormal;

	worldNormal.x = dot(i.tspace0, tnormal);
	worldNormal.y = dot(i.tspace1, tnormal);
	worldNormal.z = dot(i.tspace2, tnormal);

	fixed3 worldLight = normalize(_WorldSpaceLightPos0.xyz);
	fixed3 worldView = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xyz);
	fixed3 worldHalf = normalize(worldView + worldLight);

    fixed3 albedo = tex2D(_MainTex, i.uv.xy).rgb * _Color;
	fixed4 spec = tex2D(_SpecTex, i.uv.xy).rgba;
    fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz * albedo;
    fixed3 diffuse = _LightColor0.rgb * albedo * saturate(dot(worldNormal, worldLight));
    fixed3 specular = _LightColor0.rgb * spec.a * pow(saturate(dot(worldNormal, worldHalf)), _Shininess);

    fixed3 color = ambient + diffuse + specular;
	fixed noiseMask = tex2D(_FurMask, i.uv.xy).rgb;
    fixed alpha = tex2D(_FurTex, i.uv.zw).rgb;
    
    return fixed4(color, alpha * noiseMask);
}