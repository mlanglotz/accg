﻿Shader "Fur/FurBasicShader"
{
    Properties
    {
        _Color ("Color", Color) = (1, 1, 1, 1)
        _Shininess ("Shininess", Range(0.01, 128.0)) = 8.0
        
        _MainTex ("Texture", 2D) = "white" { }
		_FurMask("Fur Mask", 2D) = "white" { }
        _FurTex ("Fur Pattern", 2D) = "white" { }
		_SpecTex("SpecSmooth (rgb a)", 2D) = "white" { }

		_BumpMap("Normal Map", 2D) = "bump" {}
        
        _FurLength ("Fur Length", Range(0.0, 1)) = 0.5
    }

    Category
    {

        Tags { "RenderType" = "Transparent" "IgnoreProjector" = "True" "Queue" = "Transparent" "LightMode" = "ForwardBase"}
        Cull Off
        ZWrite On
        Blend SrcAlpha OneMinusSrcAlpha
        
        SubShader
        {
            Pass
			{
				CGPROGRAM

				#pragma vertex vert_surface
				#pragma fragment frag_surface
				#define FURSTEP 0.00
				#include "FurHelper.cginc"

				ENDCG

			}

			Pass
			{
				CGPROGRAM

				#pragma vertex vert_base
				#pragma fragment frag_base
				#define FURSTEP 0.25
				#include "FurHelper.cginc"

				ENDCG

			}

			Pass
			{
				CGPROGRAM

				#pragma vertex vert_base
				#pragma fragment frag_base
				#define FURSTEP 0.50
				#include "FurHelper.cginc"

				ENDCG

			}

			Pass
			{
				CGPROGRAM

				#pragma vertex vert_base
				#pragma fragment frag_base
				#define FURSTEP 0.75
				#include "FurHelper.cginc"

				ENDCG

			}

            Pass
            {
                CGPROGRAM
                
                #pragma vertex vert_base
                #pragma fragment frag_base
                #define FURSTEP 1.00
                #include "FurHelper.cginc"
                
                ENDCG
                
            }
        }
    }
}