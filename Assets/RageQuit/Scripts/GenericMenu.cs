﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GenericMenu : MenuBase
{
    public Button BackButton;
    string BackName;
    public string BackNameOverride;
    public Button NextButton;
    string NextName;
    public string NextNameOverride;
    public MenuBase NextMenu;


    public override void INIT()
    {
        base.INIT();

        BackName = BackButton.GetComponentInChildren<TMP_Text>().text;
        NextName = NextButton.GetComponentInChildren<TMP_Text>().text;

        BackButton.onClick.RemoveAllListeners();
        NextButton.onClick.RemoveAllListeners();
        BackButton.onClick.AddListener(() =>
        {
            BackButton.GetComponentInChildren<TMP_Text>().text = BackName;
            NextButton.GetComponentInChildren<TMP_Text>().text = NextName;
            Back();
        });

        NextButton.onClick.AddListener(() =>
        {
            BackButton.GetComponentInChildren<TMP_Text>().text = BackName;
            NextButton.GetComponentInChildren<TMP_Text>().text = NextName;
            Next(NextMenu);
        });

    }

    private void Start()
    {
        if(BackNameOverride.Length > 0)
        {
            BackButton.GetComponentInChildren<TMP_Text>().text = BackNameOverride;
        }

        if (NextNameOverride.Length > 0)
        {
            NextButton.GetComponentInChildren<TMP_Text>().text = NextNameOverride;
        }
    }
}
