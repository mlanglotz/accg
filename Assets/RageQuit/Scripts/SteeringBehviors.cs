﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringBehviors : MonoBehaviour
{

    public bool canMove;
    [Header("Target Values")]
    public Vector3 AreaCenterPoint; //Movement Area Center
    public float AreaRadius;
    public Transform areaCentre;
    public Vector3 MaxArea;
    public Vector3 MinArea;
    public Vector3 target; //Target for seek
    public Transform Predator;

    [Header("Steering Values")]
    public Vector3 velocity; //steering velocity
    public Vector3 steering; //final steering force
    public float MaxVelocity; //Maximum velocity magnitude
    public float MaxForce; //Maximum steer force Magnitude
    public float MaxSpeed; //Maximum movement speed;
    public float SlowingRadius;
    bool fleeing = false;
    float fleeCounter = 0;
    public float fleeTime;



    // Start is called before the first frame update
    void Start()
    {
        AreaCenterPoint = transform.position;
        ChangeTarget();
    }

    private void OnEnable()
    {
        ChangeTarget();
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            if (Predator != null)
            {
                if (Vector3.Distance(transform.position , Predator.position) < 0.3f)
                {
                    fleeing = true;
                    MaxSpeed *= 2;
                }
                else if (Vector3.Distance(transform.position , target) < 0.01)
                {
                    ChangeTarget();
                }


                if (fleeing)
                {
                    fleeCounter += Time.deltaTime;
                    if(fleeCounter >= fleeTime)
                    {
                        fleeing = false;
                        MaxSpeed /= 2;
                    }
                    Flee();
                }
                else
                {
                    Seek();
                    fleeCounter = 0;
                }
            }
            else
            {
                if (Vector3.Distance(transform.position , target) < 0.01)
                {
                    ChangeTarget();
                }
                Seek();
            }
        }
    }

    /// <summary>
    /// Seek target and move towards
    /// </summary>
    void Seek()
    {
        var position = transform.position;
        var _desiredvelocity = target - position;
        var distance = _desiredvelocity.magnitude;
        // Check the distance to detect whether the character
        // is inside the slowing area
        if (distance < SlowingRadius)
        {
            // Inside the slowing area
            _desiredvelocity = Vector3.Normalize(_desiredvelocity) * MaxVelocity * ( distance / SlowingRadius );
        }
        else
        {
            // Outside the slowing area.
            _desiredvelocity = Vector3.Normalize(_desiredvelocity) * MaxVelocity;
        }
        steering = _desiredvelocity - velocity;
        steering = Vector3.ClampMagnitude(steering , MaxForce);
        steering /= 1; //Mass

        velocity = Vector3.ClampMagnitude(velocity + steering , MaxSpeed);
        position += velocity;
        transform.position = position;
        transform.LookAt(target);
    }
    /// <summary>
    /// Inverse of Seek, run away from target
    /// </summary>
    void Flee()
    {
        var position = transform.position;
        var _desiredvelocity = position - target;
        var distance = _desiredvelocity.magnitude;

        // Check the distance to detect whether the character
        // is inside the slowing area
        if (distance < SlowingRadius)
        {
            // Inside the slowing area
            _desiredvelocity = Vector3.Normalize(_desiredvelocity) * MaxVelocity * ( distance / SlowingRadius );
        }
        else
        {
            // Outside the slowing area.
            _desiredvelocity = Vector3.Normalize(_desiredvelocity) * MaxVelocity;
        }
        steering = _desiredvelocity - velocity;
        steering = Vector3.ClampMagnitude(steering , MaxForce);
        steering /= 1; //Mass

        velocity = Vector3.ClampMagnitude(velocity + steering , MaxSpeed);
        position += velocity;
        transform.position = position;
        transform.LookAt(target);
    }

    void ChangeTarget()
    {
        AreaCenterPoint = transform.position;
        AreaCenterPoint += Vector3.one;
        var rand = Random.insideUnitSphere;
        //rand.y = Mathf.Abs(rand.y);
        //var point = Vector3.Scale(rand , AreaCenterPoint);
        var point = rand * AreaRadius;
        point.y = Mathf.Clamp(point.y , areaCentre.position.y + MinArea.y , areaCentre.position.y + MaxArea.y);
        point.x = Mathf.Clamp(point.x , areaCentre.position.x + MinArea.x , areaCentre.position.x + MaxArea.x);
        point.z = Mathf.Clamp(point.z , areaCentre.position.z + MinArea.z , areaCentre.position.z + MaxArea.z);
        target = point;
    }

    IEnumerator DelayedInactive(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        gameObject.SetActive(false);
    }
}
