﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentCameraMatcher : Singleton<ParentCameraMatcher>
{
    Camera pCam;
    Camera Cam;

    [Header("BatLightingOverides")]
    public Light skyLight;
    Color sunLight;
    public Color moonLight;
    public CameraFilterPack_Colors_Brightness cameraBrightness;

    public GameObject last;

    private void Start()
    {
        pCam = transform.parent.gameObject.GetComponent<Camera>();
        Cam = GetComponent<Camera>();
        sunLight = skyLight.color;
    }

    void Update()
    {
        Cam.fieldOfView = pCam.fieldOfView;
        Cam.farClipPlane = pCam.farClipPlane;
    }

    public void SetLight()
    {
        cameraBrightness._Brightness = 1.1f;
        skyLight.color = sunLight;
    }

    public void SetDark()
    {
        cameraBrightness._Brightness = 0.7f;
        skyLight.color = moonLight;
    }
}
