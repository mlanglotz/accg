﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GoogleDownloader : MonoBehaviour
{
    [Tooltip("The ID of the google document located afger the /d/ and before the /edit")]
    public string documentID;
    [Tooltip("The type of document: spreadsheet or document")]
    public DocumentType docType;

    string version;
    public int progress;

    public int quoteCount;

    List<string> language = new List<string>();

    public bool iOSTesting;
    string iOS_Test;
    int iOS_TestInt;
    // Start is called before the first frame update
    void Start()
    {
        if (!iOSTesting)
        {
            if (documentID.Length != 44)
            {
                Debug.LogWarning("Document ID is incorrect length");
            }
            else
            {
                DownLoadFromGoogle(documentID, docType);
            }
        }
    }

    public void ForceDownload()
    {
        DownLoadFromGoogle(documentID, docType);
    }
    public void ForceProcessing()
    {
        StartCoroutine(ProcessData(iOS_Test.Remove(0, iOS_TestInt + 1)));
    }


    public void DownLoadFromGoogle(string _docID, DocumentType _file)
    {
        string url = null;
        if (_file == DocumentType.Document)
        {
            url = "https://docs.google.com/document/d/" + _docID + "/export?format=txt";
        }
        if (_file == DocumentType.Spreadsheet)
        {
            url = "https://docs.google.com/spreadsheets/d/" + _docID + "/export?format=csv";
        }
        TimeSpan tSpan = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        int tTime = PlayerPrefs.GetInt("LastDataDownloadTime", 0);
        if (tSpan.TotalHours - tTime > 24)
        {
            StartCoroutine(Downloader.DownloadfFomLink(url, AfterDownload));
        }
        else
        {
            string tStoredData = PlayerPrefs.GetString("LastDataDownloaded", "No Data");
            if (tStoredData != "No Data")
            {
                int equalsIndex = tStoredData.IndexOf("=");
                StartCoroutine(ProcessData(tStoredData.Remove(0, equalsIndex + 1)));
            }
            else
            {
                Debug.Log("No previously stored data exists.");
            }
        }
    }

    public void AfterDownload(string _data)
    {
        if (_data == null)
        {
            Debug.LogError("Data was null after download");
            Debug.LogError("Check the file is shareable");
            string tStoredData = PlayerPrefs.GetString("LastDataDownloaded", "No Data");
            if (tStoredData != "No Data")
            {
                StartCoroutine(ProcessData(tStoredData));
            }
            else
            {
                Debug.Log("No previously stored data exists.");
            }
        }
        else
        {
            _data = _data.Substring(_data.IndexOf(System.Environment.NewLine));
            //Debug.Log("Data" + _data);
            version = _data.Substring(0, 5);
            int equalsIndex = version.IndexOf("=");
            Assert.IsFalse(equalsIndex == -1, "Could not find the '=' at the start of the version section.");

            version = version.Substring(0, equalsIndex);
            Debug.Log($"Downloaded data version: {version}");

            PlayerPrefs.SetString("LastDataDownloaded", _data);
            PlayerPrefs.SetString("LastDataDownloadVersion", version);
            TimeSpan tSpan = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            PlayerPrefs.SetInt("LastDataDownloadTime", (int)tSpan.TotalHours);

            if (!iOSTesting)
            {
                StartCoroutine(ProcessData(_data.Remove(0, equalsIndex + 1)));
            }
            else
            {
                iOS_TestInt = equalsIndex;
                iOS_Test = _data;
            }
        }
    }

    IEnumerator ProcessData(string _data)
    {
        yield return null;

        //Line Level
        int tCurrLineIndex = 0;
        bool tInQuote = false;
        int tLinesSinceUpdate = 0;
        int tLinesBetweenUpdate = 5;

        //Entry Level
        string tCurrentEntry = "";
        int tCurrCharIndex = 0;
        bool tCurrEntryContainedQuote = false;
        List<string> tCurrLineEntries = new List<string>();

        //Line endings are done differently in iOS/macOS
#if UNITY_IOS
        char tLineEnding = '\n';
        int tLineEndingLength = 1;
#else
        char tLineEnding = '\r';
        int tLineEndingLength = 2;
#endif


        while (tCurrCharIndex < _data.Length)
        {
            if (!tInQuote && (_data[tCurrCharIndex] == tLineEnding))
            {
                tCurrCharIndex += tLineEndingLength;

                if (tCurrEntryContainedQuote)
                {
                    tCurrentEntry = tCurrentEntry.Substring(1, tCurrentEntry.Length - 2);
                }

                if (tCurrentEntry != "")
                {
                    tCurrLineEntries.Add(tCurrentEntry);
                }

                tCurrentEntry = "";
                tCurrEntryContainedQuote = false;

                //Line ended
                if (tCurrLineEntries.Count > 1)
                {
                    ProccessCSVLine(tCurrLineEntries, tCurrLineIndex);
                }
                tCurrLineIndex++;
                tCurrLineEntries = new List<string>();

                tLinesSinceUpdate++;
                if (tLinesSinceUpdate > tLinesBetweenUpdate)
                {
                    tLinesSinceUpdate = 0;
                    yield return null;
                }
            }
            else
            {
                if (_data[tCurrCharIndex] == '"')
                {
                    tInQuote = !tInQuote;
                    quoteCount++;
                    tCurrEntryContainedQuote = true;
                }

                //Entry level stuff
                {
                    if (_data[tCurrCharIndex] == ',')
                    {
                        if (tInQuote)
                        {
                            tCurrentEntry += _data[tCurrCharIndex];
                        }
                        else
                        {
                            if (tCurrEntryContainedQuote)
                            {
                                tCurrentEntry = tCurrentEntry.Substring(1, tCurrentEntry.Length - 2);
                            }
                            if (tCurrentEntry != "")
                            {
                                tCurrLineEntries.Add(tCurrentEntry);
                            }
                            tCurrentEntry = "";
                            tCurrEntryContainedQuote = false;
                        }
                    }
                    else
                    {
                        string tChar = _data[tCurrCharIndex].ToString();
                        tCurrentEntry += _data[tCurrCharIndex];

                    }
                }
                tCurrCharIndex++;
            }
            progress = (int)(tCurrCharIndex / _data.Length * 100f);
        }
        if (tCurrLineEntries.Count > 1)  //Catch last line of csv
        {
            ProccessCSVLine(tCurrLineEntries, tCurrLineIndex);
            tCurrLineEntries = new List<string>();
        }
        ScriptManager.Instance.Complete();
    }

    private void ProccessCSVLine(List<string> _currLineElements, int _currLineIndex)
    {
        //First line is a special case
        if (_currLineIndex == 0)
        {
            language = new List<string>();
            for (int i = 0; i < _currLineElements.Count; i++)
            {
                string tCurrLanguage = _currLineElements[i];
                if (tCurrLanguage != "")
                {
                    Assert.IsTrue((i != 0 || tCurrLanguage == "Key"), "First column first row was: " + tCurrLanguage);
                    Assert.IsFalse(ScriptManager.Instance.languageIndicies.ContainsKey(tCurrLanguage));
                    ScriptManager.Instance.languageIndicies.Add(tCurrLanguage, i - 1);        //Use -1 as the first column is the key
                                                                                              //Debug.Log(tCurrLanguage);
                    language.Add(tCurrLanguage);
                }
            }
            Assert.IsFalse(language.Count == 0);
        }
        //All other lines use the same proccess
        else if (_currLineElements.Count > 1)
        {
            if (_currLineElements[0] == "") { return; }               //If the current documnet line has no key skip it

            string tKey = null;
            string[] tTextValues = new string[language.Count];
            for (int i = 0; i < _currLineElements.Count; i++)
            {
                if (i < language.Count)
                {
                    string tCurrentPhrase = _currLineElements[i];
                    //Debug.Log(tCurrentPhrase);
                    if (i == 0)
                    {
                        Assert.IsFalse(ScriptManager.Instance.keyTranslations.ContainsKey(tCurrentPhrase), "This key already exists: " + tCurrentPhrase + " " + _currLineIndex);
                        tKey = tCurrentPhrase;
                    }
                    else
                    {
                        tTextValues[i - 1] = tCurrentPhrase;
                        //Debug.Log(tCurrentPhrase);
                    }
                }
            }
            //Add new key and values to the translation dictionary
            ScriptManager.Instance.keyTranslations[tKey] = tTextValues;
            //Debug.Log(tKey);
        }
        else
        {
            Debug.LogError("Something has gone wrong we should'nt be here.");
        }
    }
}

public enum DownloadType
{
    DOC,
    CSV,
    TXT,
    PDF,
    XLSX
}

public enum DocumentType
{
    Spreadsheet,
    Document
}

//Source for downloader and google drive usage
//https://www.youtube.com/watch?v=niOkw_cZEiM