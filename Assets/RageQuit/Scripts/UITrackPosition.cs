﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITrackPosition : MonoBehaviour
{
    [HideInInspector]
    public Vector3 TrackingTarget;
    Camera cam;
    Vector3 pos;

    void Start()
    {
        Destroy(gameObject, 0.5f);
        cam = Camera.main;
    }

    private void Update()
    {
        if (cam != null)
        {
            pos = cam.WorldToScreenPoint(TrackingTarget);
            transform.position = pos;
        }
    }
}
