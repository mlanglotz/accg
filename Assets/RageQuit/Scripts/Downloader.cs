﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class Downloader
{
    public static IEnumerator DownloadfFomLink(string _url ,System.Action<string> onCompleted)
    {
        yield return new WaitForEndOfFrame();

        string tDownloadedData = null;

        using (UnityWebRequest webRequest = UnityWebRequest.Get(_url))
        {
            yield return webRequest.SendWebRequest();

            if(webRequest.isNetworkError)
            {
                Debug.LogError($"Download ERROR: {webRequest.error}");
            }
            else
            {
                Debug.Log("Download succesful");

                tDownloadedData = webRequest.downloadHandler.text;
            }

        }
            onCompleted(tDownloadedData);
    }
}
