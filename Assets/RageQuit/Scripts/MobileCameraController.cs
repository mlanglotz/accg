﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;

//Created by Tyrone Mills for RageQuit use in projects needing use of device camera

//TO DO: Test on iOS devices
//      Make into Singleton

public class MobileCameraController : MonoBehaviour
{
    private bool camAvailable;
    private int initiationAttempts;
    private WebCamTexture deviceCamera = null;
    WebCamDevice[] availableDevices;

    private Texture defaultBackground;

    [Tooltip("Texture to render camera capture to.")]
    public RawImage cameraFeed;
    public AspectRatioFitter fitter;

    string frontCamera;
    string backCamera;

    public Text debug;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        InitCamera();
        yield return new WaitForSeconds(1);
    }

    // Update is called once per frame
    void Update()
    {
        if(initiationAttempts > 10)
        {
            this.enabled = false;
        }
        //If the camera isn't available try to re-initialise it
        if (!camAvailable) { InitCamera(); return; }

        float tRatio = (float)(deviceCamera.width / deviceCamera.height);
        //fitter.aspectRatio = tRatio;

        float tScaleY = deviceCamera.videoVerticallyMirrored ? -1 : 1;
        cameraFeed.rectTransform.localScale = new Vector3(1, tScaleY, 1);

        int tOrient = -deviceCamera.videoRotationAngle;
        cameraFeed.rectTransform.localEulerAngles = new Vector3(0, 0, tOrient);
        debug.text = frontCamera + " " + backCamera;// deviceCamera.deviceName;
    }

    /// <summary>
    /// Author: TYrone Mills - RageQuit
    /// Description: Detects and sets active camera device
    /// </summary>
    void InitCamera()
    {
        initiationAttempts++;
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);
        }
#endif
        //defaultBackground = cameraFeed.texture;
        availableDevices = WebCamTexture.devices;

        if (availableDevices.Length == 0)
        {
            Debug.Log("No camera found");
            debug.text = "No camera found " + Permission.HasUserAuthorizedPermission(Permission.Camera);
            camAvailable = false;
            return;
        }

        for (int i = 0; i < availableDevices.Length; i++)
        {
            if (!availableDevices[i].isFrontFacing)
            {
                deviceCamera = new WebCamTexture(availableDevices[i].name, Screen.width, Screen.height);
                backCamera = availableDevices[i].name;
            }
            if (availableDevices[i].isFrontFacing)
            {
                frontCamera = availableDevices[i].name;
            }
        }
        if (deviceCamera == null)
        {
            Debug.Log("Couldn't find back camera.");
            //debug.text = "Couldn't find back camera.";
            return;
        }
        deviceCamera.Play();

        //cameraFeed.texture = deviceCamera;
        camAvailable = true;
    }

    /// <summary>
    /// Author: Tyrone Mills - RageQuit
    /// Description: Switch between camera device 0 and 1
    /// </summary>
    public void CameraFlip()
    {
        deviceCamera.Stop();
        deviceCamera.deviceName = (deviceCamera.deviceName == availableDevices[0].name) ? availableDevices[1].name : availableDevices[0].name;
        deviceCamera.Play();
    }

    public void OnEnable()
    {
        initiationAttempts = 0;
    }
}
