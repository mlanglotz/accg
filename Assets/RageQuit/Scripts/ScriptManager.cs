﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptManager : Singleton<ScriptManager>
{
    public static bool loaded = false;
    public Dictionary<string, int> languageIndicies;

    public Dictionary<string, string[]> keyTranslations;

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        languageIndicies = new Dictionary<string, int>();
        keyTranslations = new Dictionary<string, string[]>();
    }

    public string GetText(string _language, string _key)
    {
        if (keyTranslations.ContainsKey(_key) && languageIndicies.ContainsKey(_language))
        {
            int tIndicie = languageIndicies[_language];
            string tText = keyTranslations[_key][tIndicie];
            return tText;
        }
        else if(!keyTranslations.ContainsKey(_key))
        {
            Debug.LogError($"WARNING: Invalid key \"{_key}\" is being used");
            return "Information Unavailable";
        }
        else
        {
            Debug.LogError($"WARNING: Invalid language \"{_language}\" is being used");
            return "Information Unavailable";
        }
    }

    public delegate void Loaded();
    public event Loaded hasLoaded;

    public void Complete()
    {
        loaded = true;
        hasLoaded?.Invoke();
    }

    public void TestPrint()
    {
        foreach(KeyValuePair<string,string[]> i in keyTranslations)
        {
            Debug.Log(i.Value[0]);
        }
    }
}
