﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlternateUIPosition : MonoBehaviour
{
    RectTransform rect { get { return GetComponent<RectTransform>(); } }

    bool StartPos;

    Vector2 StartMinAnchor;
    Vector2 StartMaxAnchor;

    public Vector2 AltMinAnchor;
    public Vector2 AltMaxAnchor;

    private void Start()
    {
        StartMinAnchor = rect.anchorMin;
        StartMaxAnchor = rect.anchorMax;

        StartPos = true;

        ToggleAnchor(false);
    }

    public void ToggleAnchor(bool dir)
    {
        if (!dir)
        {
            rect.anchorMin = AltMinAnchor;
            rect.anchorMax = AltMaxAnchor;

            StartPos = false;
        }
        else
        {
            rect.anchorMin = StartMinAnchor;
            rect.anchorMax = StartMaxAnchor;

            StartPos = true;
        }

        rect.offsetMin = Vector2.zero;
        rect.offsetMax = Vector2.zero;
    }
}
