﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimBasic : MonoBehaviour
{
    public bool dir;
    public string anim;

    void Start()
    {
        GetComponent<Animator>().SetBool(anim,dir);   
    }
}
