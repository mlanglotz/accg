﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PureDataManager : MonoBehaviour
{
    public WaterData WD;
    public WeatherData WthrD;
    public GameObject instructions;

    public Transform WeatherHolder;
    public Transform WaterHolder;
    public GameObject NoData;

    public Text dissolvedO2;
    public Text pH;
    public Text waterTemperature;
    public Text Conductivity;
    public Text waterSampleTime;
    public Text chlorphyll;
    public Text ammonium;
    public Text chloride;

    public Text airSampleTime;
    public Text airTemperature;
    public Text humidity;
    public Text barometricPressure;

    public GameObject animals;
    public GameObject pondWater;
    Material water;

    float lostTime;
    private bool logged;
    float playTime;
    bool inFocus;

    public SoundFX GetSoundFX { get { return GetComponent<SoundFX>(); } }

    private string weatherUrl = "https://canningiot-opendata.azurewebsites.net/api/OpenQuery?code=aye4fR2fxZJREaKT8ZkIWUIeQNmd73EFPer77/iKW8haDlDEIBkGGQ==&entity=Weather";

    // Start is called before the first frame update
    void Start()
    {
        {           //Set inactive to start
            dissolvedO2.transform.parent.gameObject.SetActive(false);
            waterTemperature.transform.parent.gameObject.SetActive(false);
            pH.transform.parent.gameObject.SetActive(false);
            Conductivity.transform.parent.gameObject.SetActive(false);
            chlorphyll.transform.parent.gameObject.SetActive(false);
            chloride.transform.parent.gameObject.SetActive(false);
            ammonium.transform.parent.gameObject.SetActive(false);

            airSampleTime.transform.parent.gameObject.SetActive(false);
            airTemperature.transform.parent.gameObject.SetActive(false);
            humidity.transform.parent.gameObject.SetActive(false);
            barometricPressure.transform.parent.gameObject.SetActive(false);
        }
        EvaulateData();
        lostTime = Time.time;
        water = pondWater.GetComponent<MeshRenderer>().material;
        DataHandler.Instance.ProccessingComplete += UpdateData;
        weatherUrl = ScriptManager.Instance.GetText("English", "Weather_Data");
        StartCoroutine(Downloader.DownloadfFomLink(weatherUrl, ProcessWeatherData));

        GetComponentInParent<DefaultTrackingEvent_RQ>().LostTarget += LostTarget;
        GetComponentInParent<DefaultTrackingEvent_RQ>().GainedTarget += GainedFocus;
    }

    void Update()
    {
        if (inFocus)
        {
            playTime += Time.deltaTime;
            if (Input.touchCount > 0)
            {
                Touch tTouch = Input.GetTouch(0);
                Ray tRay = Camera.main.ScreenPointToRay(tTouch.position);
                RaycastHit[] hits = Physics.RaycastAll(tRay);
                foreach (RaycastHit r in hits)
                {
                    if (r.transform.CompareTag("Interactable"))
                    {
                        r.collider.gameObject.GetComponent<RaycastReceiver>().TriggerAnim();
                    }
                }
            }
        }
        else
        {
            if (!logged && Time.time - lostTime > 5 && playTime > 5)
            {
                FirebaseManager.Instance.LogGamePlayed("Raw_Data_Viewer", playTime);
                logged = true;
            }
        }

        if (inFocus && GetSoundFX != null)
        {

            if (!GetSoundFX.source.isPlaying)
            {
                GetSoundFX.PlaySoundLoop();
            }
        }
        else if (!inFocus && GetSoundFX != null)
        {
            if (GetSoundFX.source.isPlaying)
            {
                GetSoundFX.FadeOut();
            }
        }
    }

    public void EvaulateData()
    {
        NoData.SetActive(true);
        if (WeatherHolder.gameObject.activeInHierarchy)
        {
            foreach (Transform t in WeatherHolder)
            {
                if (t.gameObject.activeInHierarchy)
                {
                    NoData.SetActive(false);
                    break;
                }
            }
        }
        if(WaterHolder.gameObject.activeInHierarchy)
        {
            foreach (Transform t in WaterHolder)
            {
                if (t.gameObject.activeInHierarchy)
                {
                    NoData.SetActive(false);
                    break;
                }
            }
        }
    }

    void UpdateData()
    {
        if (WD != null)
        {
            dissolvedO2.text = WD.DissolvedOxygen > -50 ? ((float)WD.DissolvedOxygen).ToString("f1") + " mg/L": "Undergoing Maintainance";
            waterTemperature.text = WD.WaterTemperature > -50 ? ((float)WD.WaterTemperature).ToString("f1") + " °C": "Undergoing Maintainance";
            pH.text = "pH " + (WD.WaterPh > -50 ? ((float)WD.WaterPh).ToString("f1"): "Undergoing Maintainance");
            waterSampleTime.text = "Time Updated  " + WD._TimeValue.Substring(0,2) + ":" + WD._TimeValue.Substring(2, 2);
            Conductivity.text = WD.WaterConductivity > -50 ? ((float)WD.WaterConductivity).ToString("f1") : "Undergoing Maintainance";
            chlorphyll.text = WD.Chlorophyll > -50 ? ((float)WD.Chlorophyll).ToString("f1"): "Undergoing Maintainance";
            ammonium.text = WD.Nitrate > -50 ? ((float)WD.Nitrate).ToString("f1") : "Undergoing Maintainance"; ;
            chloride.text = WD.Chloride > -50 ? ((float)WD.Chloride).ToString("f1") : "Undergoing Maintainance"; ;
        }
        //Turn of non viable data displays
        dissolvedO2.transform.parent.gameObject.SetActive(WD.DissolvedOxygen > -50);
        waterTemperature.transform.parent.gameObject.SetActive(WD.WaterTemperature > -50);
        pH.transform.parent.gameObject.SetActive(WD.WaterPh > -50);
        Conductivity.transform.parent.gameObject.SetActive(WD.WaterConductivity > -50);
        chlorphyll.transform.parent.gameObject.SetActive(WD.Chlorophyll > -50);
        chloride.transform.parent.gameObject.SetActive(WD.Chloride > -50);
        ammonium.transform.parent.gameObject.SetActive(WD.Nitrate > -50);

        //--------------------------------


        if (WD.DissolvedOxygen > 2)
        {
            animals.SetActive(true);
        }
        //water.color = new Color(0, N2Levels.value / 300, (0.5f - N2Levels.value / 350), 0.3f + (N2Levels.value / 350));
        DataHandler.Instance.ProccessingComplete -= UpdateData;

        EvaulateData();
    }

    void ProcessWeatherData(string _data)
    {
        StartCoroutine(ProcessData(_data));
    }
    IEnumerator ProcessData(string _data)
    {
        yield return null;
        //MobileTest.text = _data;
        _data = _data.Remove(0, 1);
        _data = _data.Remove(_data.Length - 1);
        JsonUtility.FromJsonOverwrite(_data, WthrD);
        //Wait one frame to make sure processing is finished
        yield return null;

        //airSampleTime.transform.parent.gameObject.SetActive(false);
        airTemperature.transform.parent.gameObject.SetActive(WthrD.AirTemperature > -50);
        humidity.transform.parent.gameObject.SetActive(WthrD.RelativeHumidity > -50);
        barometricPressure.transform.parent.gameObject.SetActive(WthrD.BarometricPressure > -50);

        airSampleTime.text = "Time Updated  " + WthrD._TimeValue.Substring(0,2) + ":" + WthrD._TimeValue.Substring(2, 2);
        airTemperature.text = WthrD.AirTemperature.ToString("f1") + " °C";
        humidity.text = ((int)WthrD.RelativeHumidity).ToString() + "%";
        barometricPressure.text = WthrD.BarometricPressure.ToString("f1") + " hPa";
    }


    void LostTarget()
    {
        lostTime = Time.time;
        inFocus = false;
        TrackingManager.Instance.Tracked = false;
    }

    void GainedFocus()
    {
        //ParentCameraMatcher.Instance.SetLight();
        inFocus = true;
        TrackingManager.Instance.Tracked = true;
        FirebaseManager.Instance.ChangeScreen("Raw_Data_View", "Station");
        if (Time.time - lostTime > 10)
        {
            playTime = 0;
            TrackingManager.Instance.GamePlayed("PureDataPlayed");
            TrackingManager.Instance.AddStation("S01");
            instructions?.SetActive(true);
        }
    }
}
