﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Android;
using TMPro;
using System.IO;
//using Vuforia;

/// <summary>
/// Author: Tyrone Mills - RageQuit
/// </summary>
public class SelfieTaker : MonoBehaviour
{
    public GameObject instructions;
    public GameObject UI;
    public GameObject ConstUI;
    public GameObject shareCanvas;
    public Image capture;

    public Text debug;

    int width, height;

    public bool inFocus;

    float lostTime;
    public SoundFX GetSoundFX { get { return GetComponent<SoundFX>(); } }

    private int storageRequested;
    bool isProcessing;

    private string imageLocation;

    // Start is called before the first frame update
    void Start()
    {
        lostTime = Time.time;

        GetComponentInParent<DefaultTrackingEvent_RQ>().LostTarget += LostFocus;
        GetComponentInParent<DefaultTrackingEvent_RQ>().GainedTarget += GainedFocus;

        width = Screen.width;
        height = Screen.height;
    }

    void Update()
    {
        if(inFocus)
        {
            if(storageRequested < 3 && !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
            {
                //TO DO: pop dialog
                string msg = "Please let use the storage device else I can't save your photos.";
            }
            if (Input.touchCount > 0)
            {
                Touch tTouch = Input.GetTouch(0);
                Ray tRay = Camera.main.ScreenPointToRay(tTouch.position);
                RaycastHit[] hits = Physics.RaycastAll(tRay);
                foreach (RaycastHit r in hits)
                {
                    if (r.transform.CompareTag("Interactable"))
                    {
                        r.collider.gameObject.GetComponent<RaycastReceiver>().TriggerAnim();
                    }
                }
            }
        }
    }

    public void ButFirst()
    {
        StartCoroutine(Click());
    }


    IEnumerator Click()
    {
        UI.SetActive(false);
        ConstUI?.SetActive(false);
        string tName = "Christmas" + System.DateTime.Now.ToString("yyyy-mm-dd_H:mm:ss");
        yield return new WaitForEndOfFrame();
        Texture2D snap = new Texture2D(width, height, TextureFormat.RGB24, false);
        snap.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        snap.Apply();

        capture.sprite = Sprite.Create(snap, new Rect(0,0, snap.width, snap.height), new Vector2(0,0));

        yield return new WaitForEndOfFrame();
        UI.SetActive(true);
        ConstUI?.SetActive(true);
        imageLocation = tName + ".jpg";
        File.WriteAllBytes(Path.Combine(Application.temporaryCachePath, imageLocation), snap.EncodeToPNG());
        NativeGallery.Permission p = NativeGallery.SaveImageToGallery(snap, "SmartCanning", tName + ".jpg", Saved);
        debug.gameObject.SetActive(true);
        if (p == NativeGallery.Permission.Granted)
        {
            debug.text = "SAVED";
        }
        else if (p == NativeGallery.Permission.Denied)
        {
            debug.text = "Permission needs to be given to save images. Check the apps permissions in settings.";
        }
    }

    /// <summary>
    /// Author: Tyrone Mills - RageQuit
    /// Description: Refreshes the Gallery to include new screenshot
    /// </summary>
    /// <param name="_path"></param>
    void Saved(string _path)
    {
        //imageLocation = _path;
        //debug.text = _path;
        shareCanvas.SetActive(true);
    }

    public void PostToTwitter()
    {

    }

    public void PostToFacebook()
    {

        //if (NativeShare.TargetExists("com.facebook.katana"))
        {
            debug.text = Application.temporaryCachePath + imageLocation;   //Cant seem to find file and errors out.
            new NativeShare().AddFile(Path.Combine(Application.temporaryCachePath, imageLocation)).SetTitle("Exploring Smart Canning").SetTarget("").Share();
            debug.text = "Facebook Shared";
        }
    }

#if UNITY_ANDROID
    IEnumerator ShareScreenshot(string _source)
    {
        isProcessing = true;

        //string destination = Path.Combine(Application.persistentDataPath, "screenshotroadies.png");

        if (!Application.isEditor)
        {
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
            AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + _source);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "I am testing stuff");
            intentObject.Call<AndroidJavaObject>("setType", "image/jpg");
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share your new score");
            currentActivity.Call("startActivity", chooser);

        }
        isProcessing = false;
        return null;
    }
#endif

    void GainedFocus()
    {
        //ParentCameraMatcher.Instance.SetLight();
        TrackingManager.Instance.Tracked = true;
        inFocus = true;
        FirebaseManager.Instance.ChangeScreen("Selfie_Station", "Station");
        if (Time.time - lostTime > 10)
        {
            TrackingManager.Instance.GamePlayed("SelfieFrame");
            TrackingManager.Instance.AddStation("S06");
            instructions?.SetActive(true);
        }
        if (GetSoundFX != null)
        {

            if (!GetSoundFX.source.isPlaying)
            {
                GetSoundFX.PlaySoundLoop();
            }
        }

        //TO DO: Request permission for storage here.
        //AndroidStoragePermissionRequest();
    }

    public void AndroidStoragePermissionRequest()
    {
#if UNITY_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            storageRequested++;
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        }

#endif
    }

    void LostFocus()
    {
        lostTime = Time.time;
        inFocus = false;
        TrackingManager.Instance.Tracked = false;
        if (GetSoundFX != null)
        {
            if (GetSoundFX.source.isPlaying)
            {
                GetSoundFX.FadeOut();
            }
        }
    }
}
