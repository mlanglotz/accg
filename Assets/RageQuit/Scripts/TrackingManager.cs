﻿using UnityEngine;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

public class TrackingManager : Singleton<TrackingManager>
{
    public GameObject trackingLost;
    public GameObject leavePrompt;
    public GameObject screenMap;

    static bool tracking = false;

    float untrackedTime;

    private const string AndroidRatingURI = "http://play.google.com/store/apps/details?id={0}";

    string orderPlayed;

    public void Start()
    {
        int tPlayed = PlayerPrefs.GetInt("TimesPlayed", 0);
        tPlayed++;
        PlayerPrefs.SetInt("TimesPlayed", tPlayed);
    }

    public bool Tracked
    {
        get { return tracking; }
        set
        {
            tracking = value;
            if(tracking)
            {
                untrackedTime = 0;
                leavePrompt.SetActive(false);
            }
            TrackStateChanged?.Invoke(tracking);
            trackingLost.SetActive(!tracking);
            screenMap.SetActive(!tracking);
        }
    }


    void Update()
    {
        if(!tracking)
        {
            untrackedTime += Time.deltaTime;

            if(untrackedTime > 180)
            {
                leavePrompt.SetActive(true);
            }
        }
    }

    public void ResetTime()
    {
        untrackedTime = 0;
    }

    public delegate void GlobalTrackedEvent(bool _newState);
    public event GlobalTrackedEvent TrackStateChanged;

    public void GamePlayed(string _game)
    {
        int tPlayCount = PlayerPrefs.GetInt(_game, 0);
        PlayerPrefs.SetInt(_game, tPlayCount++);
    }

    public void AddStation(string _station)
    {
        orderPlayed += _station;
    }

    void RequestAppReview()
    {
#if UNITY_IOS
        if(!Device.RequestStoreReview())
        {
            //TO DO: request user go to store page and review Apps
        }

#elif UNITY_ANDROID
        string url = AndroidRatingURI.Replace("{0}", Application.identifier);

        if (!string.IsNullOrEmpty(url))
        {
            Application.OpenURL(url);
        }
        else
        {
            Debug.LogWarning("Unable to open URL, invalid OS");
        }
#endif
    }

    public void ThumbsUp()
    {
        RequestAppReview();
    }

    public void ThumbsDown()
    {
        //TO DO; something for suppport.
    }

    public void OnApplicationQuit()
    {
        FirebaseManager.Instance.LogStationsPlayed(orderPlayed);
    }
}
