﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconDisplay : MonoBehaviour
{
    public bool Visible;
    public List<Animator> IconPositions;

    private void Start()
    {
        Toggle();
    }

    public void Toggle()
    {
        Visible = !Visible;
        StartCoroutine("Visibility");
    }

    IEnumerator Visibility ()
    {
        WaitForSeconds wait = new WaitForSeconds(0.025f);
        foreach (Animator i in IconPositions)
        {
            i.SetBool("Visible", Visible);
            yield return wait;
        }
    }
}
