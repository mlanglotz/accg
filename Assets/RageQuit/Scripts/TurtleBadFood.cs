﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurtleBadFood : MonoBehaviour
{

    public float swirlRate;
    public float speed;

    public Vector3 heading;
    float desiredZ;
    float lastChange;

    public Vector3 bounds;

    public Vector3 debug;
    // Start is called before the first frame update
    void Start()
    {
        heading.x = speed;
    }

    // Update is called once per frame
    void Update()
    {
        if(lastChange > swirlRate)
        {
            Swirl();
            lastChange = 0;
        }
        lastChange += Time.deltaTime;
        heading.z = Mathf.MoveTowards(heading.z, desiredZ, 0.001f);

        transform.position += heading;
        if(transform.position.z < -bounds.z)
        {
            Vector3 tPos = transform.position;
            tPos.z = -bounds.z;
            transform.position = tPos;
            heading.z = 0.05f;
        }else if (transform.position.z > bounds.z)
        {
            Vector3 tPos = transform.position;
            tPos.z = bounds.z;
            transform.position = tPos;
            heading.z = -0.05f;
        }
        if(transform.position.x < -bounds.x)
        {
            Vector3 tPos = transform.position;
            tPos.x = bounds.x;
            transform.position = tPos;
        }
        debug = transform.position;
        if (transform.position.y < bounds.y)
        {
            Vector3 tPos = transform.position;
            tPos.y = bounds.y;
            transform.position = tPos;
        }
    }

    void Swirl()
    {
        desiredZ = Random.Range(-0.05f, 0.05f);
    }

    IEnumerator DelayedInactive(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        gameObject.SetActive(false);
    }
}
