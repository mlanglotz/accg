﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatGameManager : Singleton<BatGameManager>
{
    public MiniGameState MGS;

    public FlappyWorld FW;

    public float TotalScore;
    public GameObject instructions;
    public GameObject infoPanel;
    public Text score;
    public GameObject gameFinished;
    public GameObject ragdoll;
    [HideInInspector]
    public GameObject rd; // Ragdoll to delete
    float lostTime;
    [SerializeField]
    bool inFocus;
    public bool IsFocused { get { return inFocus; } }

    private bool logged;
    float playTime;

    // Start is called before the first frame update
    void Start()
    {
        lostTime = Time.time;
        score.text = "0";
        score.transform.parent.gameObject.SetActive(false);
        FW.Logic.GetRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        GetComponentInParent<DefaultTrackingEvent_RQ>().LostTarget += LostTarget;
        GetComponentInParent<DefaultTrackingEvent_RQ>().GainedTarget += GainedFocus;
        FW.Logic.Dead += Died;
    }

    // Update is called once per frame
    void Update()
    {
        if (inFocus)
        {
            //if(!instructions.activeInHierarchy)
            {
                score.transform.parent.gameObject.SetActive(!instructions.activeInHierarchy);
                score.text = ((int)TotalScore).ToString();
            }
            playTime += Time.deltaTime;
        }
        else
        {
            if (!logged && Time.time - lostTime > 5 && playTime > 5)
            {
                FirebaseManager.Instance.LogGamePlayed("Santa_Game", playTime);
                logged = true;
            }
        }
    }

    void LostTarget()
    {
        //ParentCameraMatcher.Instance.SetLight();
        if (rd != null) Destroy(rd); // Delete the ragdoll
        FW.Logic.GetRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        lostTime = Time.time;
        inFocus = false;
        MGS.timeScale = 0;
        TrackingManager.Instance.Tracked = false;
    }

    void GainedFocus()
    {
        //ParentCameraMatcher.Instance.SetDark();
        //FW.Logic.GetRigidbody.constraints = RigidbodyConstraints.None;
            FirebaseManager.Instance.ChangeScreen("Santa_Game", "Station");
        if (Time.time - lostTime > 10)
        {
            playTime = 0;
            FW.StartGame();
            ResetGame();
            instructions?.SetActive(true);
            score.transform.parent.gameObject.SetActive(false);
            TrackingManager.Instance.GamePlayed("SantaGamePlayed");
            TrackingManager.Instance.AddStation("S03");
        }
        TrackingManager.Instance.Tracked = true;
        inFocus = true;
        MGS.timeScale = 1;
    }

    public void ResetGame()
    {
        if (rd != null) Destroy(rd); // Delete the ragdoll
        TotalScore = 0;
        FW.Logic.PS.ActivateScore(0);
    }

    public void Died()
    {
        gameFinished.SetActive(true);
        var go = Instantiate(ragdoll, FW.Logic.transform.position, FW.Logic.transform.rotation);
        go.transform.SetParent(transform.parent);
        rd = go;
        FW.Logic.gameObject.SetActive(false);
        score.transform.parent.gameObject.SetActive(false);
    }
}
