﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimRandomStart : MonoBehaviour
{

    public Animation Animation { get { return GetComponentInChildren<Animation>(); } }

    public Animator Animator { get { return GetComponentInChildren<Animator>(); } }

    // Start is called before the first frame update
    void Start()
    {
        if(Animation != null)
        {
            Animation[Animation.clip.name].time = Random.Range(0f, Animation.clip.length);
        }

        if (Animator != null)
        {
            AnimatorStateInfo state = Animator.GetCurrentAnimatorStateInfo(0);
            Animator.Play(state.fullPathHash, -1, Random.Range(0f, 1f));
        }
    }
}
