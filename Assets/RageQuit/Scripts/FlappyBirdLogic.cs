﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(GestureManager))]
[RequireComponent(typeof(Rigidbody))]
public class FlappyBirdLogic : MonoBehaviour
{

    public GestureManager gestures { get { return GetComponent<GestureManager>(); } } //For Tap
    public Rigidbody GetRigidbody { get { return GetComponent<Rigidbody>(); } } //Rigidbody getter
    public Animator GetAnimator { get { return GetComponentInChildren<Animator>(); } }
    public SoundFX GetSoundFX { get { return GetComponent<SoundFX>(); } }


    public float Force; // Force used in flap
    public float Speed; // Speed of the world translation
    public float MaxFlapHeight; // Self explanitory
    public float MaxVelocity = 10; // Self explanitory
    public Vector3 debugvel;

    public bool dead;   // am i ded
    public float cooldown = 0.5f;   // flap cooldown(anti spam)
    float cd = 0.5f;    // flap cooldown timer
    bool canflap = true;    // anti spam
    public float SpeedupTime;   // time till world speed up
    float spdt = 0; // speedup timer
    public float startingSpeed;

    bool gliding = false;
    float glidet = 0;
    public float glideTransitionSpeed = 1;
    float flapt = 0;
    bool flapping = false;

    public MiniGameState MGS;

    public PopScore PS;
    /// <summary>
    /// Collectable hit event
    /// </summary>
    /// <param name="collectable"></param>
    public delegate void OnFlappyCollectable(FlappyCollectable collectable);
    public event OnFlappyCollectable CollectableHit;


    public delegate void OnFlappyDeath();
    public event OnFlappyDeath Dead;

    // Start is called before the first frame update
    void Start()
    {
        gestures.Tapper += OnTap;
        GetAnimator.enabled = false;
        startingSpeed = Speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (!dead)
        {
            cd += Time.deltaTime;
            if (cd > cooldown)
            {
                canflap = true;
            }

            spdt += Time.deltaTime;
            if (spdt >= SpeedupTime)
            {
                Speed -= 1f;
                spdt = 0;
            }
            flapt += Time.deltaTime;
            if (flapt > 0.5f)
            {
                glidet += glideTransitionSpeed * Time.deltaTime;
                var lerp = Mathf.Lerp(0 , 0.95f , glidet);
                GetAnimator.SetLayerWeight(1 , lerp);
            }
        }
        debugvel = GetRigidbody.velocity;
    }

    /// <summary>
    /// Tap Event listener
    /// </summary>
    void OnTap()
    {
        if (BatGameManager.Instance.IsFocused && !BatGameManager.Instance.instructions.activeInHierarchy && !BatGameManager.Instance.infoPanel.activeInHierarchy)
        {
            if (canflap && !dead)
            {
                Flap();
                GetSoundFX.PlaySoundOneShot();
            }
        }
    }
    /// <summary>
    /// Add force upwards
    /// </summary>
    void Flap()
    {
        if (!dead)
        {
            GetRigidbody.constraints = RigidbodyConstraints.None;
            GetRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            canflap = false;
            cd = 0;
            if (transform.localPosition.z < MaxFlapHeight)
            {
                if (GetRigidbody.velocity.y < MaxVelocity)
                {
                    GetRigidbody.AddForce(new Vector3(0 , Force , 0) , ForceMode.Impulse);
                }
            }
            if (!GetAnimator.enabled)
            {
                GetAnimator.enabled = true;
            }
            GetAnimator.SetBool("ForceFlap" , true);
            GetAnimator.SetLayerWeight(1 , 0);
            flapt = 0;
            glidet = 0;
        }
    }

    /// <summary>
    /// On collision if collectable do thing, if not die
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.GetComponent<FlappyCollectable>())
        {
            CollectableHit.Invoke(collision.collider.GetComponent<FlappyCollectable>());
            GetSoundFX.PlaySoundOneShot("CollectionSnd1"); //play collection sound
        }
        else
        {
            dead = true;
            GetAnimator.enabled = false;
            Dead?.Invoke();
            GetSoundFX.PlaySoundOneShot("BatDeathSnd1"); //play death sound
            PS.ActivateScore(BatGameManager.Instance.TotalScore);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<FlappyCollectable>())
        {
            CollectableHit.Invoke(other.GetComponent<FlappyCollectable>());
            GetSoundFX.PlaySoundOneShot("CollectionSnd1"); //play collection sound
        }
        else
        {
            dead = true;
            Dead?.Invoke();
            GetSoundFX.PlaySoundOneShot("BatDeathSnd1"); //play death sound
            PS.ActivateScore(BatGameManager.Instance.TotalScore);
        }
    }


}
