﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Water Data", menuName = "RageQuit/Water Data", order = 10)]
public class WaterData : ScriptableObject
{
    public string id;
    public string PartitionKey;
    public string DeviceID;
    public string ObservationTimestamp;
    public float BatteryLevel;
    public float DissolvedOxygen;
    public float WaterConductivity;
    public float WaterPh;
    public float WaterTemperature;
    public float Chlorophyll;
    public float Chloride;
    public float Nitrate;
    public string _DataValue;
    public string _TimeValue;
    public string _rid;
    public string _self;
    public string _etag;
    public string _attachments;
    public string _ts;

    public void SetToNULL()
    {
        DissolvedOxygen = -270;
        WaterConductivity = -270;
        WaterPh = -270;
        WaterTemperature = -270;
        Chlorophyll = -270;
        Chloride = -270;
        Nitrate = -270;
    }
}
