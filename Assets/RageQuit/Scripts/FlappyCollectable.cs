﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappyCollectable : MonoBehaviour
{
    public int Points;
    public float BoidLerpStartPosition;
    public float BoidLerpTargetPosition;
    public SpawnHeight height;
}
