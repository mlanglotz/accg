﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericToggle : MonoBehaviour
{
    public bool OneActive;
    public List<GameObject> SetOne;

    [Space]
    public bool TwoActive;
    public List<GameObject> SetTwo;

    private void Start()
    {
        if (!OneActive)
        {
            foreach (GameObject g in SetOne)
            {
                g.SetActive(false);
            }
        }
        if (!TwoActive)
        {
            foreach (GameObject g in SetTwo)
            {
                g.SetActive(false);
            }
        }
    }

    public void Toggle()
    {
        foreach(GameObject g in SetOne)
        {
            g.SetActive(!g.activeInHierarchy);
        }

        foreach (GameObject g in SetTwo)
        {
            g.SetActive(!g.activeInHierarchy);
        }
    }
}
