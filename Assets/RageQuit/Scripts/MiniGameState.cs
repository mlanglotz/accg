﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Minigame State", menuName = "RageQuit/Games/States", order = 10)]
public class MiniGameState : ScriptableObject
{
    public float timeScale;

    public bool played;
    public float score;

}

//VUFORIA
//https://www.youtube.com/watch?v=MtiUx_szKbI