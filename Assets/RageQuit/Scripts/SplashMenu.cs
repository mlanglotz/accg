﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Hosting;
using UnityEngine;
using UnityEngine.UI;

public class SplashMenu : MenuBase
{
    public Button BackButton;
    public Button NextButton;
    public MenuBase NextMenu;


    public override void INIT()
    {
        base.INIT();
        BackButton.onClick.RemoveAllListeners();
        NextButton.onClick.RemoveAllListeners();
        BackButton.onClick.AddListener(() =>
        {
            Application.Quit();
        });

        NextButton.onClick.AddListener(() =>
        {
            Next(NextMenu);
        });

    }

    private void Start()
    {
        BackButton.GetComponentInChildren<TMPro.TMP_Text>().text = "Exit";
    }
}
