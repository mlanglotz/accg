﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurtleFoodMover : MonoBehaviour
{
    public float speed;
    public Vector3 heading;

    public Vector3 bounds;

    public Transform foods;
    List<Transform> fallingFoods =  new List<Transform>();

    public GameObject[] possibleFoods;

    public Vector3 fallRate;

    public float addRate;
    int foodCount;
    float lastAdd;

    float durationMod;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        if (TurtleGameManager.Instance.IsFocused)
        {
            if (foods.childCount < 5 && lastAdd > addRate)
            {
                AddFood();
                speed += 0.15f;

            }
            lastAdd += Time.deltaTime;
            MoveFoods();
            FallDown();
        }
    }


    void AddFood()
    {
        int randLayer = Random.Range(0, 3);
        int rand = Random.Range(0, possibleFoods.Length);
        var tGO = Instantiate(possibleFoods[rand], foods);
        tGO.transform.SetParent(foods);
        tGO.transform.position = new Vector3(bounds.x, -(randLayer * 7), 0);
        foodCount++;
        lastAdd = 0;
    }

    void MoveFoods()
    {
        foreach (Transform t in foods)
        {
            t.position += heading * speed * Time.deltaTime;
            if (t.position.y > -1)
            {
                if (t.position.x < -(bounds.x - 33))
                {
                    if (!fallingFoods.Contains(t))
                    {
                        fallingFoods.Add(t);
                    }
                }
            }
            else
            {
                if (t.position.x < -bounds.x)
                {
                    if (fallingFoods.Contains(t))
                    {
                        fallingFoods.Remove(t);
                    }
                    Destroy(t.gameObject);
                    foodCount--;
                }
            }
        }
    }

    void FallDown()
    {
        foreach(Transform t in fallingFoods)
        {
            t.position -= fallRate * Time.deltaTime;
        }
    }

    IEnumerator DelayedInactive(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        gameObject.SetActive(false);
    }
}
