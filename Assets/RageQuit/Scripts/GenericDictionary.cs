﻿using UnityEngine;
using UnityEngine.Video;

/// <summary>
/// Generic dictionary class
/// </summary>
/// <typeparam name="T">The value</typeparam>
[System.Serializable]
public class GenericDictionary<T, V>
{
    public T key;
    public V _Value;
}
[System.Serializable]
public class SoundObject: GenericDictionary<string, AudioClip>
{

}