﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HungryHipposLogic : MonoBehaviour
{
    //public Button TriggerBot;
    //public Button TriggerMid;
    //public Button TriggerTop;
    public Transform foods;
    public Transform Player;
    public List<Transform> Eaten;
    public bool AreaTest;
    public float MaxDistance;
    public float AreaWidthMax;
    public float AreaWidthMin;
    public float AreaLength;
    public bool debugLines;

    public Vector2 biteZone;

    public float strickRate;
    private float lastStrick;
    private float activated;

    TurtleGameManager TGM { get { return TurtleGameManager.Instance; } }

    Animator turtleAnim { get { return GetComponentInChildren<Animator>(); } } //For turtle animations;

    void Start()
    {

    }


    private void Update()
    {
        if (debugLines)
        {
            if (Player)
            {
                var l1 = Player.position;
                l1.x += AreaWidthMin;

                var l1end = l1;
                l1end.z += AreaLength;

                Debug.DrawLine(l1, l1end, Color.red, Time.deltaTime);


                var l2 = Player.position;
                l2.x += AreaWidthMax;

                var l2end = l2;
                l2end.z += AreaLength;
                Debug.DrawLine(l2, l2end, Color.red, Time.deltaTime);
            }

            foreach (Transform t in foods)
            {
                if (t.gameObject.activeInHierarchy)
                {
                    var _temp = Vector3.Distance(t.position, Player.position);

                    if (!AreaTest)
                    {
                        if (_temp < MaxDistance)
                        {
                            Debug.DrawLine(Player.position, t.position, Color.red);
                        }
                    }
                }
            }
        }
        if (TGM.IsFocused && !TGM.instructions.activeInHierarchy && !TGM.infoPanel.activeInHierarchy && !TGM.gameFinished.activeInHierarchy)
        {
            if (Input.touchCount > 0)
            {
                Touch tap = Input.GetTouch(0);
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(tap.position);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject.name == "BiteHeightTop")
                    {
                        Hit(0);
                    }
                    else if (hit.collider.gameObject.name == "BiteHeightBot")
                    {
                        Hit(-1);
                    }
                    else if (hit.collider.gameObject.name == "BiteHeightMid")
                    {
                        Hit(-0.5f);
                    }
                }
            }
            if(Input.GetMouseButtonDown(0))
            {
                Vector2 tMouse = Input.mousePosition;
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(tMouse);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject.name == "BiteHeightTop")
                    {
                        Hit(0);
                    }
                    else if (hit.collider.gameObject.name == "BiteHeightBot")
                    {
                        Hit(-1);
                    }
                    else if (hit.collider.gameObject.name == "BiteHeightMid")
                    {
                        Hit(-0.5f);
                    }
                }
            }
        }
        lastStrick += Time.deltaTime;
    }

    /// <summary>
    /// Check every boid and see which is in area, then from the ones in area hit the closest one
    /// </summary>
    void Hit(float _height)
    {
        if (lastStrick > strickRate)
        {
            GameObject _closest = null;
            var _d = MaxDistance + 1;

            foreach (Transform t in foods)
            {
                if (t.position.x > biteZone.x && t.position.x < biteZone.y)
                {

                    if (_height == 0 && t.position.y > -0.5)
                    {
                        _closest = t.gameObject;
                    }
                    else
                    if (_height == -0.5f && (t.position.y > -8 && t.position.y < -6))
                    {
                        _closest = t.gameObject;
                    }
                    else
                    if (_height == -1 && (int)t.position.y == -14)
                    {
                        _closest = t.gameObject;
                    }
                    //break;
                }
            }

                turtleAnim.SetTrigger("Eat");
                turtleAnim.SetFloat("X-Axis", 0);
                turtleAnim.SetFloat("Y-Axis", _height);
            if (_closest != null)
            {
                Vector3 tPos = turtleAnim.gameObject.transform.InverseTransformPoint(_closest.transform.position); ;

                Debug.DrawLine(turtleAnim.transform.position, tPos, Color.green, 5);
                _closest.GetComponent<OnTurtleEat>().StartActiveDelay(false, 0.4f);
                //Destroy(_closest);
            }
            lastStrick = 0;
        }
    }

    /// <summary>
    /// Check to see if point falls inbetween two lines
    /// </summary>
    /// <param name="position">the point</param>
    /// <returns>if its within the two lines' range</returns>
    bool InRange(Vector3 position)
    {
        //can check if out of range immediately by using abs z comparison
        if (Mathf.Abs(position.z) > Mathf.Abs(AreaLength))
        {
            return false;
        }

        var l1 = Player.position;
        l1.x += AreaWidthMin;

        var l2 = Player.position;
        l2.x += AreaWidthMax;

        //check if its within X axis area
        if (position.x > l1.x && position.x < l1.x)
        {
            //check that its on the same side as the area
            if (position.z < 0 && AreaLength < 0)
            {
                return true;
            }
            else if (position.z > 0 && AreaLength > 0)
            {
                return true;
            }
        }
        return false;
    }

}
