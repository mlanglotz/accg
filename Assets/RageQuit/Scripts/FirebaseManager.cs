﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Analytics;


////https://canningwsb.firebaseio.com/

public class FirebaseManager : Singleton<FirebaseManager>
{
    // Start is called before the first frame update
    void Start()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            if (task.Exception != null)
            {
                Debug.LogError($"Firebase failed to initialise with {task.Exception}");
                return;
            }
            else
            {
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                Debug.Log($"Firebase initialised {task.Result}");
            }
        });

        //Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
        //    FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
        //    FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelStart, new Parameter(FirebaseAnalytics.ParameterLevelName, "Start"));

        //    var dependencyStatus = task.Result;
        //    if (dependencyStatus == Firebase.DependencyStatus.Available)
        //    {
        //        // Create and hold a reference to your FirebaseApp,
        //        // where app is a Firebase.FirebaseApp property of your application class.

        //        var app = Firebase.FirebaseApp.DefaultInstance;
        //    }
        //    else
        //    {
        //        UnityEngine.Debug.LogError(System.String.Format(
        //        "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
        //        // Firebase Unity SDK is not safe to use here.
        //    }
        //});
    }

    public void LogEvent(string _game)
    {
        FirebaseAnalytics.LogEvent(_game);
    }

    public void ChangeScreen(string _name, string _type)
    {
        FirebaseAnalytics.SetCurrentScreen(_name, _type);
    }

    public void LogGameStart(string _game)
    {
       FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelStart, new Parameter[] { new Parameter("Level_Name", _game) });
    }

    public void LogGamePlayed(string _game, float _duration)
    {
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelEnd, new Parameter[] { new Parameter("Level_Name", _game), new Parameter("Level_Duration", _duration) });
    }

    public void LogButtonEvent(string _button)
    {
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventSelectContent, new Parameter[] { new Parameter("Button_Name", _button) });
    }

    public void LogStationsPlayed(string _stations)
    {
        if (_stations.Length > 2)
        {
            FirebaseAnalytics.LogEvent("custom_station_order", new Parameter[] { new Parameter("Play_Order", _stations) });
        }
    }

    public void LogRatingofApp(int _rating)
    {
        FirebaseAnalytics.LogEvent("cutom_rating_event", new Parameter[] { new Parameter("Rating", _rating) });
    }

    /// <summary>
    /// Not really FireBase relalated but not sure where to put this.
    /// </summary>
    public void GoToWebsite()
    {
        string tSite = ScriptManager.Instance.GetText("English", "Website");
        Application.OpenURL(tSite);
    }

    public void OnApplicationQuit()
    {
        //TO DO: sende session loggins analytics
    }
}


////API: https://firebase.google.com/docs/reference/unity/namespace/firebase/analytics?utm_referrer=unity