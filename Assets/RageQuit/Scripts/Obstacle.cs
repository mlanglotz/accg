﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public bool CanSpawnStart;
    public SpawnHeight height;
    public FlappyCollectable onMe;
}
