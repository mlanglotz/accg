﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "Sound Library" , menuName = "RageQuit/Sound Library")]
public class SoundLibrary : ScriptableObject
{
    public List<SoundObject> AllSounds;

    public AudioClip GetSound(string name)
    {
        if (!AllSounds.Any(item => item.key == name))
        {
            return null;
        }
        else
        {
            return AllSounds.Where(item => item.key == name).FirstOrDefault()._Value;
        }
    }
}
