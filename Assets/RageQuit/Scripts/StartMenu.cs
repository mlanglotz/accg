﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenu : MenuBase
{
    public Button BackButton;
    public Button NextButton;
    public MenuBase NextMenu;
    public GameObject MainUI;

    public override void INIT()
    {
        base.INIT();
        BackButton.onClick.RemoveAllListeners();
        NextButton.onClick.RemoveAllListeners();
        BackButton.GetComponentInChildren<TMPro.TMP_Text>().text = "Go Back";
        BackButton.onClick.AddListener(() =>
        {
            Back();
        });

        NextButton.onClick.AddListener(() =>
        {
            //NextNoPrev(NextMenu);
            NextMenu.gameObject.SetActive(true);
            StartAR();
        });

    }

    public void StartAR()
    {
        //TO DO: Request Camera permission
        CameraAndroidRequest();
        AndroidStoragePermissionRequest();
        // MainUI.SetActive(false);
        SceneManager.LoadScene(1);
    }

    public void CameraAndroidRequest()
    {
#if UNITY_ANDROID
        if(!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);

        }
#endif
    }

    public void AndroidStoragePermissionRequest()
    {
#if UNITY_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        }

#endif
    }
}
