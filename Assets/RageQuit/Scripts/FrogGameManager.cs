﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class FrogGameManager : Singleton<FrogGameManager>
{
    public MiniGameState MGS;

    public FroggyWorld FW;
    public GameObject instructions;

    public GameObject infoPanel;

    public SoundFX GetSoundFX { get { return GetComponent<SoundFX>(); } }

    public float TotalScore;

    public int winningScore;

    public TMP_Text score;
    public TMP_Text time;

    public Transform boids;
    public GameObject gameFinished;
    public TMP_Text messageTitle;
    public TMP_Text message;
    float lostTime;
    [SerializeField]
    bool inFocus;
    public bool IsFocused { get { return inFocus; } }// = true; // _____________ Forced on
    bool reset = true;

    private bool logged;
    float playTime;
    float stationDuration;

    public PopScore PS;
    public GameObject ragdoll;
    [HideInInspector]
    public GameObject rd; // Ragdoll to delete

    //public Text debug;
    // Start is called before the first frame update
    void Start()
    {
        
        lostTime = Time.time;
        score.text = "0";
        score.transform.parent.gameObject.SetActive(false);
        GetComponentInParent<DefaultTrackingEvent_RQ>().LostTarget += LostTarget;
        GetComponentInParent<DefaultTrackingEvent_RQ>().GainedTarget += GainedFocus;
    }

    void Update()
    {
        if (inFocus)
        {
            playTime += Time.deltaTime;
            stationDuration += Time.deltaTime;

            score.transform.parent.gameObject.SetActive(!instructions.activeInHierarchy);
            if (!instructions.activeInHierarchy && !gameFinished.activeInHierarchy)
            {
                time.text = TimeSpan.FromSeconds(60 - playTime).ToString("mm\\:ss");
                if (playTime > 60 && !gameFinished.activeInHierarchy)
                {
                    gameFinished.SetActive(true);
                    PS.ActivateScore(TotalScore);
                    messageTitle.text = "Well Done";
                    message.text = $"Congratulations your score was: {TotalScore}";
                    score.transform.parent.gameObject.SetActive(false);
                    FW.Logic.dead = true;
                }
            }
        }
        else
        {
            if (!logged && Time.time - lostTime > 5 && stationDuration > 5)
            {
                FirebaseManager.Instance.LogGamePlayed("Rudolph_Game", stationDuration);
                logged = true;
            }
        }

        if (inFocus && GetSoundFX != null)
        {

            if (!GetSoundFX.source.isPlaying)
            {
                GetSoundFX.PlaySoundLoop();
            }
        }
        else if (!inFocus && GetSoundFX != null)
        {
            if (GetSoundFX.source.isPlaying)
            {
                GetSoundFX.FadeOut();
            }
        }
    }

    public void BugEaten(float _score, Vector3 _pos)
    {
        ScreenEffects.Instance.ScoreEffect(_score, _pos);
        TotalScore += _score;
        if(TotalScore < 0)
        {
            FW.Logic.dead = true;
            gameFinished.SetActive(true);
            PS.ActivateGameOver();
            messageTitle.text = ScriptManager.Instance.GetText("English", "S04_Try_Again_Title");
            message.text = ScriptManager.Instance.GetText("English", "S04_Try_Again");
        }
        score.text = TotalScore.ToString();
    }

    void LostTarget()
    {
        lostTime = Time.time;
        inFocus = false;
        MGS.timeScale = 0;
        reset = false;
        TrackingManager.Instance.Tracked = false;
        if (rd != null) Destroy(rd); // Delete the ragdoll
        //debug.text = "Focus lost";
    }

    void GainedFocus()
    {
        //ParentCameraMatcher.Instance.SetLight();
        inFocus = true;
        MGS.timeScale = 1;
        TrackingManager.Instance.Tracked = true;
        FirebaseManager.Instance.ChangeScreen("Rudolph_Game", "Station");
        if (Time.time - lostTime > 10)
        {
            playTime = 0;
            stationDuration = 0;
            TrackingManager.Instance.GamePlayed("RudolphGamePlayed");
            TrackingManager.Instance.AddStation("S04");
            instructions?.SetActive(true);
            score.transform.parent.gameObject.SetActive(false);
            ResetGame();
        }
        //debug.text = "Focus gained";
    }

    public void ResetGame()
    {
        TotalScore = 0;
        playTime = 0;
        score.text = TotalScore.ToString();
        gameFinished.SetActive(false);
        reset = true;
        FW.StartGame();
        if (rd != null) Destroy(rd); // Delete the ragdoll
    }
}
