﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInterpolateOnActive : MonoBehaviour
{
    RectTransform rect;

    bool dir;
    float current = 0;

    // Start is called before the first frame update
    void Awake()
    {
        rect = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (dir)
        {
            current += Time.deltaTime * 8;
            if (current > 1)
            {
                current = 1;
            }
        }
        else
        {
            current -= Time.deltaTime * 8;
            if (current < 0)
            {
                current = 0;
            }
        }

        float perc = current / 1;

        rect.localScale = new Vector3(rect.localScale.x, perc, rect.localScale.z);
    }

    public void Open()
    {
        rect.localScale = new Vector3(rect.localScale.x, 0, rect.localScale.z);
        current = 0;
        dir = true;
    }
    public void Close()
    {
        dir = false;
    }
}
