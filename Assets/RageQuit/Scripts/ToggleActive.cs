﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ToggleActive : MonoBehaviour
{
    Button button;
    public GameObject go;
    public bool startActive = false;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        
        button.onClick.AddListener( ()=> { ToggleFunction(); });

        go.SetActive(startActive);
    }

    void ToggleFunction()
    {
        Debug.Log("Switch");
        go.SetActive(!go.activeInHierarchy);
    }
}
