﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FroggyWorld : MonoBehaviour
{
    public Transform ObstaclesTransform;
    public Transform GroundTransform;
    public Transform BoidsTransform;
    public List<GameObject> ObstacleObjects;
    public List<GameObject> BoidObjects;
    public MiniGameState MGS;
    public float WorldMoveSpeed;
    public List<GameObject> land;
    public float deleteRange;
    public List<Vector3> GroundStartPositions;
    public Vector3 StartPosition;
    public FrogLogic Logic;
    float t;
    bool jump = false;
    bool ready = true;
    List<float> GroundLerpTargetPositions;
    List<float> GroundLerpStartPositions;

    List<float> ObstacleLerpTargetPositions;
    List<float> ObstacleLerpStartPositions;

    public float BoidsChance; //0-100
    public float ObstacleChance; //0-100

    private void Awake()
    {
        MGS.score = 0;
    }

    private void Start()
    {
        //Logic.dead = true;
        Logic.GetRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        Logic.GetRigidbody.angularVelocity = Vector3.zero;
        Logic.GetRigidbody.velocity = Vector3.zero;
        Logic.JumpTriggered += Move;
        Logic.CollectableHit += CollectableHit;
        StartPosition = Logic.transform.localPosition;
        GroundStartPositions = new List<Vector3>();
        GroundLerpStartPositions = new List<float>();
        GroundLerpTargetPositions = new List<float>();

        ObstacleLerpTargetPositions = new List<float>();
        ObstacleLerpStartPositions = new List<float>();

        foreach (Transform item in GroundTransform)
        {
            GroundStartPositions.Add(item.localPosition);
        }
    }

    private void CollectableHit(FlappyCollectable collectable)
    {
        //MGS.score += 1;
        Destroy(collectable.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (FrogGameManager.Instance.IsFocused)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                StartGame();
            }
            if (ready)
            {
                    t += ( Logic.jumpSpeed / 2 ) * Time.deltaTime * MGS.timeScale;
                if (jump && !Logic.dead)
                {
                    for (int i = 0; i < GroundTransform.childCount; i++)
                    {
                        var p = GroundTransform.GetChild(i).localPosition;
                        var lerp = Mathf.Lerp(GroundLerpStartPositions[i] , GroundLerpTargetPositions[i] , t);
                        p.x = lerp;
                        GroundTransform.GetChild(i).localPosition = p;
                    }
                    foreach (Transform item in BoidsTransform)
                    {
                        var p = item.localPosition;
                        var lerp = Mathf.Lerp(item.GetComponent<FlappyCollectable>().BoidLerpStartPosition , item.GetComponent<FlappyCollectable>().BoidLerpTargetPosition , t);
                        p.x = lerp;
                        item.localPosition = p;
                    }
                    for (int i = 0; i < ObstaclesTransform.childCount; i++)
                    {
                        var p = ObstaclesTransform.GetChild(i).localPosition;
                        var lerp = Mathf.Lerp(ObstacleLerpStartPositions[i] , ObstacleLerpTargetPositions[i] , t);
                        p.x = lerp;
                        ObstaclesTransform.GetChild(i).localPosition = p;
                    }
                    if (t > 1f)
                    {
                        t = 0;
                        jump = false;
                        foreach (Transform item in GroundTransform)
                        {
                            if (item.localPosition.x < deleteRange)
                            {
                                if (GroundTransform.childCount <= 5)
                                {
                                    for (int i = 0; i < 1; i++)
                                    {
                                        Spawn();
                                    }
                                }
                                else
                                {
                                    Spawn();
                                }
                                Destroy(item.gameObject);
                            }
                        }

                        if (ObstaclesTransform.childCount > 0)
                        {
                            foreach (Transform item in ObstaclesTransform)
                            {
                                if (item.localPosition.x < deleteRange)
                                {
                                    Destroy(item.gameObject);
                                }
                            }

                        }
                        if (BoidsTransform.childCount > 0)
                        {
                            foreach (Transform item in BoidsTransform)
                            {
                                if (item.localPosition.x < ( deleteRange / 100 ))
                                {
                                    Destroy(item.gameObject);
                                }
                            }
                        }
                        GroundLerpStartPositions.Clear();
                        GroundLerpTargetPositions.Clear();
                        ObstacleLerpTargetPositions.Clear();
                        ObstacleLerpStartPositions.Clear();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Starts the game
    /// </summary>
    public void StartGame()
    {
        Logic.PS.ActivateScore(0);
        StartCoroutine(ActuallyStart());
    }

    //Wait a frame
    IEnumerator ActuallyStart()
    {
        ready = false;
        jump = false;
        yield return new WaitForFixedUpdate();
        foreach (Transform item in ObstaclesTransform)
        {
            Destroy(item.gameObject);

        }
        foreach (Transform item in GroundTransform)
        {
            Destroy(item.gameObject);
        }
        foreach (Transform item in BoidsTransform)
        {
            Destroy(item.gameObject);
        }

        var pos = GroundStartPositions[0];
        var a = Random.Range(0 , land.Count);
        var l = Instantiate(land[a] , GroundTransform);
        l.transform.localPosition = pos;

        for (int i = 0; i < 4; i++)
        {
            SpawnStart();
        }
        yield return new WaitForFixedUpdate();
        ready = true;
        Logic.gameObject.SetActive(true);
        Logic.dead = false;
        Logic.transform.localPosition = StartPosition;
    }


    public void Move(float MovementRange)
    {
        for (int i = 0; i < GroundTransform.childCount; i++)
        {
            GroundLerpTargetPositions.Add(0);
            GroundLerpStartPositions.Add(0);
        }
        for (int i = 0; i < ObstaclesTransform.childCount; i++)
        {
            ObstacleLerpTargetPositions.Add(0);
            ObstacleLerpStartPositions.Add(0);
        }


        for (int i = 0; i < GroundTransform.childCount; i++)
        {
            var pos = GroundTransform.GetChild(i).localPosition;
            GroundLerpTargetPositions[i] = pos.x - MovementRange;
            GroundLerpStartPositions[i] = pos.x;
        }
        foreach (Transform item in BoidsTransform)
        {
            var pos = item.localPosition;
            item.GetComponent<FlappyCollectable>().BoidLerpTargetPosition = pos.x - ( MovementRange );
            item.GetComponent<FlappyCollectable>().BoidLerpStartPosition = pos.x;
        }
        for (int i = 0; i < ObstaclesTransform.childCount; i++)
        {
            var pos = ObstaclesTransform.GetChild(i).localPosition;
            ObstacleLerpTargetPositions[i] = pos.x - MovementRange;
            ObstacleLerpStartPositions[i] = pos.x;
        }
        jump = true;
        t = 0;
    }

    public void Spawn()
    {

        //Spawn Ground
        var pos = GroundTransform.GetChild(GroundTransform.childCount - 1).localPosition;
        pos.x += 40f;
        var a = Random.Range(0 , land.Count);
        var l = Instantiate(land[a] , GroundTransform);
        l.transform.localPosition = pos;

        bool boids = false;//if boids then no obstacle
        if (BoidObjects.Count > 0)
        {
            var r = Random.Range(0 , 100);
            if (r < BoidsChance)
            {
                boids = true;
                var rand = Random.Range(0 , BoidObjects.Count);
                var obs = Instantiate(BoidObjects[rand] , BoidsTransform);
                var newpos = obs.transform.localPosition;
                newpos.x = pos.x / 100; //scale of boids is wild
                obs.transform.localPosition = newpos;
            }
        }
        if (ObstacleObjects.Count > 0)
        {
            if (!boids)
            {
                var r = Random.Range(0 , 100);
                if (r < ObstacleChance)
                {
                    var rand = Random.Range(0 , ObstacleObjects.Count);

                    var obs = Instantiate(ObstacleObjects[rand] , ObstaclesTransform);
                    var newpos = obs.transform.localPosition;
                    newpos.x = pos.x;
                    obs.transform.localPosition = newpos;
                }
            }
        }

    }

    public void SpawnStart()
    {
        //Spawn Ground
        var pos = GroundTransform.GetChild(GroundTransform.childCount - 1).localPosition;
        pos.x += 40f;
        var a = Random.Range(0 , land.Count);
        var l = Instantiate(land[a] , GroundTransform);
        l.transform.localPosition = pos;

        if (BoidObjects.Count > 0)
        {
            var r = Random.Range(0 , 100);
            if (r < BoidsChance)
            {
                var rand = Random.Range(0 , BoidObjects.Count);
                var obs = Instantiate(BoidObjects[rand] , BoidsTransform);
                var newpos = obs.transform.localPosition;
                newpos.x = pos.x / 100; //scale of boids is wild
                obs.transform.localPosition = newpos;
            }
        }
    }

}
