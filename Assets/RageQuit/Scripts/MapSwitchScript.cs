﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class MapSwitchScript : MonoBehaviour
{
    Button button;

    public GameObject mapCanvas;

    public List<GameObject> worldToHide;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();

        button.onClick.AddListener(ToggleActives);
    }

    public void ToggleActives()
    {
        mapCanvas.SetActive(!mapCanvas.activeInHierarchy);

        foreach(GameObject g in worldToHide)
        {
            g.SetActive(!mapCanvas.activeInHierarchy);
        }
    }
}
