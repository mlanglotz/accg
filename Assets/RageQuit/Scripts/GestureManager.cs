﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureManager : MonoBehaviour
{

    Touch touch;
    Vector2 beginTouchPosition;
    Vector2 endTouchPosition;

    bool down; //editor only
    [SerializeField]
    float length;
    [SerializeField]
    float angle;
    public float TapSensitivity;
    /// <summary>
    /// Swipe event
    /// </summary>
    public delegate void OnSwipe(Vector2 start , Vector2 end);
    public event OnSwipe Swiper;

    /// <summary>
    /// Tap Event
    /// </summary>
    public delegate void OnTap();
    public event OnTap Tapper;

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    beginTouchPosition = touch.position;
                    break;
                case TouchPhase.Ended:
                    endTouchPosition = touch.position;
                    GestureEnded();
                    break;
                default:
                    break;
            }
        }

#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            down = true;
            beginTouchPosition = new Vector2(Input.mousePosition.x , Input.mousePosition.y);
        }
        if (down)
        {
            if (Input.GetMouseButtonUp(0))
            {
                down = false;
                endTouchPosition = new Vector2(Input.mousePosition.x , Input.mousePosition.y);
                GestureEnded();
            }
        }
#endif
    }

    void GestureEnded()
    {
        length = Vector2.Distance(beginTouchPosition , endTouchPosition);
        var dir = endTouchPosition - beginTouchPosition;

        angle = Vector2.Angle(Vector2.right , dir);

        if (length > 5)
        {
            if (Swiper != null)
            {
                Swiper.Invoke(beginTouchPosition , endTouchPosition);
            }
        }

        ///use any sort of interaction with the screen = a tap cause it seems to be iffy on swipe and tap or my fingers are fat :|
        if (Tapper != null)
        {
            Tapper.Invoke();
        }
    } 
}
