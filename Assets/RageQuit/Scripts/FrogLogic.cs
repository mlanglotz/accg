﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GestureManager))]
public class FrogLogic : MonoBehaviour
{
    public float[] JumpHeight; // Self explanitory
    public float[] SwipeLengths; //differentiation of swipes by length -- 0 medium jump swipe, 1 Large jump swipe
    public float[] SwipeAngles; //differentiation of swipes by length -- 0 medium jump swipe, 1 Large jump swipe
    public float[] MovementRanges;
    public Rigidbody GetRigidbody { get { return GetComponent<Rigidbody>(); } } //Rigidbody getter
    public float GroundHeight; //where the ground is located
    public float JumpCooldown; //Time until can jump again
    float jpc = 0;
    float currentHeight; //current Lerp Target height
    public bool dead;   // am i ded
    bool onGround = true;    // can only jump when on ground
    bool jump = false; //am i jumping
    bool ascending = false; //Ascended super saiyan 
    float t; //for lerp
    public float jumpSpeed; //Lerp jump speed
    public MiniGameState MGS; //game state
    public GestureManager gestures { get { return GetComponent<GestureManager>(); } } //For swiping
    public SoundFX GetSoundFX { get { return GetComponent<SoundFX>(); } }
    Animator frogAnim { get { return GetComponentInChildren<Animator>(); } } //For frog animations
    public bool LostTracking = false;
    public bool useSwipeAngle = false;

    public PopScore PS;
    public Transform waist;

    /// <summary>
    /// Collectable hit event
    /// </summary>
    /// <param name="collectable"></param>
    public delegate void OnFlappyCollectable(FlappyCollectable collectable);
    public event OnFlappyCollectable CollectableHit;

    /// <summary>
    /// Jump event
    /// </summary>
    public delegate void OnJump(float Range);
    public event OnJump JumpTriggered;


    // Start is called before the first frame update
    void Start()
    {
        gestures.Swiper += OnSwipe;
        GroundHeight = transform.localPosition.z;
    }

    // Update is called once per frame
    void Update()
    {
        if (FrogGameManager.Instance.IsFocused)
        {
            jpc += Time.deltaTime * MGS.timeScale;
            if (jump)
            {
                onGround = false;
                t += jumpSpeed * Time.deltaTime * MGS.timeScale;
                if (ascending) //lerp to current target
                {
                    var p = transform.localPosition;
                    var lerp = Mathf.Lerp(GroundHeight, currentHeight, t);
                    p.z = lerp;
                    transform.localPosition = p;

                    if (t > 1f)
                    {
                        p.z = currentHeight;
                        transform.localPosition = p;
                        ascending = false;
                        t = 0;
                    }
                }
                else //fall to ground
                {

                    var p = transform.localPosition;
                    var lerp = Mathf.Lerp(currentHeight, GroundHeight, t);
                    p.z = lerp;
                    transform.localPosition = p;
                    if (t > 1f)
                    {
                        ascending = false;
                        p.z = GroundHeight;
                        transform.localPosition = p;
                        jump = false;
                        onGround = true;
                        t = 0;
                        GetSoundFX.PlaySoundOneShot("FrogLandSnd1"); //play swipe sound
                        frogAnim.SetFloat("Speed", 0.0f);
                    }
                }
            }
        }
    }


    /// <summary>
    /// Add force upwards
    /// </summary>
    void Jump(int index)
    {
        if (!LostTracking)
        {
            if (!dead)
            {
                if (onGround)
                {
                    if (jpc > JumpCooldown)
                    {
                        currentHeight = JumpHeight[index];
                        jump = true;
                        ascending = true;
                        t = 0;
                        jpc = 0;
                        JumpTriggered.Invoke(MovementRanges[index]);
                        switch (index)
                        {
                            case 0:
                                frogAnim.SetFloat("Speed", 0.5f);
                                break;
                            case 1:
                                frogAnim.SetTrigger("Jump");
                                break;
                            case 2:
                                frogAnim.SetTrigger("Jump");
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// On collision if collectable do thing, if not die
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.GetComponent<FlappyCollectable>())
        {
            CollectableHit.Invoke(collision.collider.GetComponent<FlappyCollectable>());
            if(collision.collider.GetComponent<OnDEathPoints>().scoreAmount > 0)
            {
                GetSoundFX.PlaySoundOneShot("CollectionSnd1"); //play collection sound
            }
            else
            {
                GetSoundFX.PlaySoundOneShot("CollectionSnd2"); //play collection sound bad
            }
        }
        else
        {
            if (collision.collider.CompareTag("obstacle"))
            {
                dead = true;
                GetSoundFX.PlaySoundOneShot("BatDeathSnd1"); //play death sound
                PS.ActivateGameOver();
                FrogGameManager.Instance.gameFinished.SetActive(true);
                FrogGameManager.Instance.score.transform.parent.gameObject.SetActive(false);
                var go = Instantiate(FrogGameManager.Instance.ragdoll, waist.position, waist.rotation);
                go.transform.SetParent(transform.parent);
                FrogGameManager.Instance.rd = go;
                gameObject.SetActive(false);
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<FlappyCollectable>())
        {
            CollectableHit.Invoke(other.GetComponent<FlappyCollectable>());
            if (other.GetComponent<OnDEathPoints>().scoreAmount > 0)
            {
                GetSoundFX.PlaySoundOneShot("CollectionSnd1"); //play collection sound
            }
            else
            {
                GetSoundFX.PlaySoundOneShot("CollectionSnd2"); //play collection sound bad
            }
        }
        else
        {
            if (other.CompareTag("obstacle"))
            {
                dead = true;
                GetSoundFX.PlaySoundOneShot("BatDeathSnd1"); //play death sound
                FrogGameManager.Instance.gameFinished.SetActive(true);
                FrogGameManager.Instance.score.transform.parent.gameObject.SetActive(false);
                PS.ActivateScore(MGS.score);
            }
        }
    }

    public void OnSwipe(Vector2 start , Vector2 end)
    {
        var Length = Vector2.Distance(start , end);
        var dir = end - start;
        var angle = Vector2.Angle(Vector2.right , dir);
        if (FrogGameManager.Instance.IsFocused && !FrogGameManager.Instance.instructions.activeInHierarchy && !FrogGameManager.Instance.infoPanel.activeInHierarchy)
        {
            GetSoundFX.PlaySoundOneShot("FrogSwipeSnd1"); //play swipe sound
            if (useSwipeAngle)
            {
                if (angle > 0 && angle < SwipeAngles[0]) //small swipe
                {
                    Jump(0);
                   
                }
                else if (angle > SwipeAngles[0] && angle < SwipeAngles[1]) //medium swipe
                {
                    Jump(1);
                  
                }
                else if (angle > SwipeAngles[1]) //large swipe
                {
                    Jump(2);
                
                }
            }
            else
            {
                if (Length > 0 && Length < SwipeLengths[0]) //small swipe
                {
                    Jump(0); 
                   // frogAnim.SetTrigger("EatLow");
                }
                else if (Length > SwipeLengths[0] && Length < SwipeLengths[1]) //medium swipe
                {
                    Jump(1);
                  //  frogAnim.SetTrigger("EatMid");
                }
                else if (Length > SwipeLengths[1]) //large swipe
                {
                    Jump(2);
                   // frogAnim.SetTrigger("EatHigh");
                }
            }
        }
    }

}
