﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class RaycastReceiver : MonoBehaviour
{
    public string animTriggerName;

    public void TriggerAnim()
    {
        if(animTriggerName != "")
        {
            GetComponent<Animator>()?.SetTrigger(animTriggerName);
        }

        if(GetComponent<DirectorControlPlayable>().director != null)
        {
            GetComponent<DirectorControlPlayable>().director.Play();
        }
    }
}
