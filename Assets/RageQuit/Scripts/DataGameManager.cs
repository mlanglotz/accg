﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class DataGameManager : MonoBehaviour
{
    public WaterData HEWD;
    public WaterData WD;
    public WaterData LEWD;
    public MiniGameState MGS;

    public GameObject instructions;

    [Space]
    public GameObject pondDeath;
    bool diedOnce = false;
    [Space]
    public TMP_Text OxygenText;
    public TMP_Text TemperatureText;
    public TMP_Text NitrateText;
    public TMP_Text PHText;
    public TMP_Text ChlorophylText;
    public TMP_Text ChlorideText;

    [Header("Sliders")]
    public Slider O2Levels;
    public Slider N2Levels;
    public Slider Temperature;
    public Slider chloride;
    public Slider chlorophyll;
    public Slider pH;

    public Slider fertiliser;
    public Slider birdFeed;
    public Slider lawnClippings;

    [Header("Animal Controls")]
    public GameObject animals;
    public GameObject animals2;
    public GameObject bugs;
    public GameObject bugs2;
    public GameObject pondWater;
    public GameObject nitrogenPlants;
    public GameObject nitrogenPlants2;
    public GameObject basePlants;
    Material water;

    public TextOverrider TO;

    //Tracking values to determine change in value
    private float prevTemp;
    private float prevFertiliser;
    private float prevFeed;
    private float prevClippings;
    private float prevN2;
    private float prevChloro;

    private Color startColour;
    public Color finalColor;

    private bool logged;
    float playTime;
    float lostTime;

    bool inFocus;

    public SoundFX GetSoundFX { get { return GetComponent<SoundFX>(); } }

    float totalHumanActions;
    // Start is called before the first frame update
    void Start()
    {
        lostTime = Time.time;
        //DataHandler.Instance.ProccessingComplete += UpdateData;           //TO DO: turn into button
        pondDeath.gameObject.SetActive(false);

        GetComponentInParent<DefaultTrackingEvent_RQ>().LostTarget += LostTarget;
        GetComponentInParent<DefaultTrackingEvent_RQ>().GainedTarget += GainedFocus;

        water = pondWater.GetComponent<MeshRenderer>().material;
        startColour = water.color;

        DefaultIdeals();
    }

    void Update()
    {
        if (inFocus)
        {
            playTime += Time.deltaTime;
            if (Input.touchCount > 0)
            {
                Touch tTouch = Input.GetTouch(0);
                Ray tRay = Camera.main.ScreenPointToRay(tTouch.position);
                RaycastHit[] hits = Physics.RaycastAll(tRay);
                foreach (RaycastHit r in hits)
                {
                    if (r.transform.CompareTag("Interactable"))
                    {
                        r.collider.gameObject.GetComponent<RaycastReceiver>().TriggerAnim();
                    }
                }
            }
        }
        else
        {
            if (!logged && Time.time - lostTime > 5 && playTime > 5)
            {
                FirebaseManager.Instance.LogGamePlayed("Data_Game", playTime);
                logged = true;
            }
        }

        if (inFocus && GetSoundFX != null)
        {

            if (!GetSoundFX.source.isPlaying)
            {
                GetSoundFX.PlaySoundLoop();
            }
        }
        else if (!inFocus && GetSoundFX != null)
        {
            if (GetSoundFX.source.isPlaying)
            {
                GetSoundFX.FadeOut();
            }
        }
    }

    void DefaultIdeals()
    {
        N2Levels.value = 6.7f;                                          //TO DO: replace with live data
        Temperature.value = 20;
        prevTemp = Temperature.value;
        chloride.value = 5;                                             //TO DO: replace with live data
        chlorophyll.value = 1;                                          //TO DO: replace with live data
        O2Levels.value = 7;
        pH.value = 6.5f;

        O2Levels.handleRect.GetComponent<Image>().color = Color.green;
        N2Levels.handleRect.GetComponent<Image>().color = Color.green;
        Temperature.handleRect.GetComponent<Image>().color = Color.green;
        chloride.handleRect.GetComponent<Image>().color = Color.green;
        chlorophyll.handleRect.GetComponent<Image>().color = Color.green;
        pH.handleRect.GetComponent<Image>().color = Color.green;

        O2Levels.onValueChanged.AddListener(delegate { OxygenUpdate(); });
        N2Levels.onValueChanged.AddListener(delegate { NitrogenUpdate(); });
        Temperature.onValueChanged.AddListener(delegate { TemperatureChange(); });
        chloride.onValueChanged.AddListener(delegate { ChlorideChange(); });
        chlorophyll.onValueChanged.AddListener(delegate { ChlorophyllChange(); });
        pH.onValueChanged.AddListener(delegate { pHUpdate(); });

        fertiliser.onValueChanged.AddListener(delegate { FertiliserUpdate(); });
        birdFeed.onValueChanged.AddListener(delegate { FeedUpdate(); });
        lawnClippings.onValueChanged.AddListener(delegate { ClippingsUpdate(); });

        FertiliserUpdate();
        FeedUpdate();
        ClippingsUpdate();
    }

    public void UpdateData()
    {
        O2Levels.onValueChanged.AddListener(delegate { OxygenUpdate(); });
        N2Levels.onValueChanged.AddListener(delegate { NitrogenUpdate(); });
        Temperature.onValueChanged.AddListener(delegate { TemperatureChange(); });
        chloride.onValueChanged.AddListener(delegate { ChlorideChange(); });
        chlorophyll.onValueChanged.AddListener(delegate { ChlorophyllChange(); });
        pH.onValueChanged.AddListener(delegate { pHUpdate(); });

        fertiliser.onValueChanged.AddListener(delegate { FertiliserUpdate(); });
        birdFeed.onValueChanged.AddListener(delegate { FeedUpdate(); });
        lawnClippings.onValueChanged.AddListener(delegate { ClippingsUpdate(); });

        FertiliserUpdate();
        FeedUpdate();
        ClippingsUpdate();

        water = pondWater.GetComponent<MeshRenderer>().material;
        startColour = water.color;
        if (WD != null)
        {
            N2Levels.value = (WD.Nitrate > -50) ? (float)WD.Nitrate : 6.7f;
            Temperature.value = (WD.WaterTemperature > -50) ? (float)WD.WaterTemperature : 20;
            prevTemp = Temperature.value;
            chloride.value = (WD.Chloride > -50) ? (float)WD.Chloride : 6.5f; 
            chlorophyll.value = (WD.Chlorophyll > -50) ? (float)WD.Chlorophyll : 1; 
            O2Levels.value = WD.DissolvedOxygen > -50 ? (float)WD.WaterPh : 7;
            pH.value = (WD.WaterPh > -50) ? (float)WD.WaterPh : 6.5f;
        }

        //DataHandler.Instance.ProccessingComplete -= UpdateData;
    }


    void OxygenUpdate()
    {
        if (O2Levels.value < 2f)
        {
            if (!diedOnce)
            {
                PondDeath();
            }
            O2Levels.handleRect.GetComponent<Image>().color = Color.red;
        }
        else if (O2Levels.value >= 2f && O2Levels.value < 4f)
        {
            O2Levels.handleRect.GetComponent<Image>().color = Color.yellow;
        }
        else
        {
            O2Levels.handleRect.GetComponent<Image>().color = Color.green;
        }
        EvaluatePond();
        OxygenText.text = "Oxygen (mg/L) " + O2Levels.value.ToString("f1");
        if (ScriptManager.loaded && TO?.key != "S02_Dissolved_Oxygen")
        {
            TO?.SetKeyandUpdate("S02_Dissolved_Oxygen");
        }
    }

    void NitrogenUpdate()
    {
        if(totalHumanActions > 5 && N2Levels.value < 90)
        {
            N2Levels.value = 90;
        }
        if (N2Levels.value < 10)
        {
            N2Levels.handleRect.GetComponent<Image>().color = Color.green;

        }
        else if (N2Levels.value >= 10 && N2Levels.value <= 100)
        {
            N2Levels.handleRect.GetComponent<Image>().color = Color.yellow;
        }
        else if (N2Levels.value > 100)
        {
            N2Levels.handleRect.GetComponent<Image>().color = Color.red;
        }
        pH.value += (N2Levels.value - prevN2) / 150;
        chlorophyll.value += (N2Levels.value - prevN2) / 8;
        prevN2 = N2Levels.value;
        NitrateText.text = "Nitrates (ug/L) " + N2Levels.value.ToString("f1");
        if (ScriptManager.loaded && TO?.key != "S02_Nitrate")
        {
            TO?.SetKeyandUpdate("S02_Nitrate");
        }
    }

    void TemperatureChange()
    {
        if (Temperature.value == 0)
        {
            Temperature.handleRect.GetComponent<Image>().color = Color.red;
        }
        else
        if (Temperature.value < 10)
        {
            Temperature.handleRect.GetComponent<Image>().color = Color.yellow;
        }
        else if (Temperature.value >= 10 && Temperature.value <= 25)
        {
            Temperature.handleRect.GetComponent<Image>().color = Color.green;
        }
        else if (Temperature.value > 25)
        {
            Temperature.handleRect.GetComponent<Image>().color = Color.yellow;
        }
        O2Levels.value += (prevTemp - Temperature.value) / 5; // 1 - (1/ (WD.WaterTemperature/Temperature.value))
        N2Levels.value -= (prevTemp - Temperature.value) / 10;
        prevTemp = Temperature.value;
        EvaluatePond();
        TemperatureText.text = "Temperature (°C) " + Temperature.value.ToString("f1");
        if(ScriptManager.loaded && TO?.key != "S02_Temperature")
        {
            TO?.SetKeyandUpdate("S02_Temperature");
        }
    }

    void ChlorideChange()
    {
        if (chloride.value <= 300)
        {
            chloride.handleRect.GetComponent<Image>().color = Color.green;
        }
        else if ((chloride.value > 300 && chloride.value < 1000))
        {
            chloride.handleRect.GetComponent<Image>().color = Color.yellow;
        }
        else if (chloride.value >= 1000)
        {
            chloride.handleRect.GetComponent<Image>().color = Color.red;
            if (!diedOnce)
            {
                PondDeath();
            }
        }
        EvaluatePond();
        ChlorideText.text = "Chloride (mg/L) " + chloride.value.ToString("f1");
        if (ScriptManager.loaded && TO?.key != "S02_Chloride")
        {
            TO?.SetKeyandUpdate("S02_Chloride");
        }
    }

    void ChlorophyllChange()
    {
        if(N2Levels.value > 100 && chlorophyll.value < prevChloro)
        {
            chlorophyll.value = prevChloro;
        }
        if (chlorophyll.value < 10)
        {
            chlorophyll.handleRect.GetComponent<Image>().color = Color.green;
            water.color = startColour;
        }
        else if (chlorophyll.value >= 10 && chlorophyll.value <= 30)
        {
            chlorophyll.handleRect.GetComponent<Image>().color = Color.yellow;
            water.color = Color.Lerp(startColour, finalColor, chlorophyll.value / chlorophyll.maxValue);
        }
        else if (chlorophyll.value > 30)
        {
            water.color = Color.Lerp(startColour, finalColor, chlorophyll.value / chlorophyll.maxValue);
            chlorophyll.handleRect.GetComponent<Image>().color = Color.red;
            if (!diedOnce)
            {
                PondDeath();
            }
        }
        EvaluatePond();
        pH.value += (chlorophyll.value - prevChloro) / 50;
        prevChloro = chlorophyll.value;
        ChlorophylText.text = "Chlorophyll (ug/L) " + chlorophyll.value.ToString("f1");
        if (ScriptManager.loaded && TO?.key != "S02_Chlorophyll")
        {
            TO?.SetKeyandUpdate("S02_Chlorophyll");
        }
    }

    void pHUpdate()
    {
        if (pH.value >= 5 && pH.value <= 7)
        {
            pH.handleRect.GetComponent<Image>().color = Color.green;
        }
        else if ((pH.value < 5 && pH.value > 4) || (pH.value < 10 && pH.value > 7))
        {
            pH.handleRect.GetComponent<Image>().color = Color.yellow;
        }
        else if (pH.value >= 10 || pH.value <= 4)
        {
            pH.handleRect.GetComponent<Image>().color = Color.red;
            if (!diedOnce)
            {
                PondDeath();
            }
        }
        EvaluatePond();
        PHText.text = "pH " + pH.value.ToString("f1");
        if (ScriptManager.loaded && TO?.key != "S02_pH")
        {
            TO?.SetKeyandUpdate("S02_pH");
        }
    }

    void FertiliserUpdate()
    {
        N2Levels.value -= 60 * (prevFertiliser - fertiliser.value);
        if (fertiliser.value < 1)
        {
            fertiliser.handleRect.GetComponent<Image>().color = Color.green;
        }
        else if (fertiliser.value < 3)
        {
            fertiliser.handleRect.GetComponent<Image>().color = Color.yellow;
        }
        else
        {
            fertiliser.handleRect.GetComponent<Image>().color = Color.red;
        }
        prevFertiliser = fertiliser.value;
        if (ScriptManager.loaded && TO?.key != "S02_Fertiliser_Info")
        {
            TO?.SetKeyandUpdate("S02_Fertiliser_Info");
        }
        totalHumanActions = lawnClippings.value + birdFeed.value + fertiliser.value;
    }

    void FeedUpdate()
    {
        N2Levels.value -=60 * (prevFeed - birdFeed.value);
        O2Levels.value += (prevFeed - birdFeed.value);
        if (birdFeed.value < 1)
        {
            birdFeed.handleRect.GetComponent<Image>().color = Color.green;
        }
        else if (birdFeed.value < 3)
        {
            birdFeed.handleRect.GetComponent<Image>().color = Color.yellow;
        }
        else
        {
            birdFeed.handleRect.GetComponent<Image>().color = Color.red;
        }
        prevFeed = birdFeed.value;
        if (ScriptManager.loaded && TO?.key != "S02_Feeding_Info")
        {
            TO?.SetKeyandUpdate("S02_Feeding_Info");
        }
        totalHumanActions = lawnClippings.value + birdFeed.value + fertiliser.value;
    }

    void ClippingsUpdate()
    {
        N2Levels.value -= 60 * (prevClippings - lawnClippings.value);
        O2Levels.value += (prevClippings - lawnClippings.value);
        if (lawnClippings.value < 1)
        {
            lawnClippings.handleRect.GetComponent<Image>().color = Color.green;
        }
        else if (lawnClippings.value < 3)
        {
            lawnClippings.handleRect.GetComponent<Image>().color = Color.yellow;
        }
        else
        {
            lawnClippings.handleRect.GetComponent<Image>().color = Color.red;
        }
        prevClippings = lawnClippings.value;
        if (ScriptManager.loaded && TO?.key != "S02_Lawn_Info")
        {
            TO?.SetKeyandUpdate("S02_Lawn_Info");
        }
        totalHumanActions = lawnClippings.value + birdFeed.value + fertiliser.value;
    }

    void EvaluatePond()
    {
        if (O2Levels.value > 4 && chloride.value < 250 && chlorophyll.value < 30 && (pH.value > 4 && pH.value < 10) && Temperature.value > 5)
        {
            animals.SetActive(true);
        }
        else
        {
            animals.SetActive(false);
        }
        if (O2Levels.value > 6 && chloride.value < 450 && chlorophyll.value < 30 && (pH.value > 4 && pH.value < 10) && Temperature.value > 5)
        {
            animals2?.SetActive(true);
        }
        else
        {
            animals2?.SetActive(false);
        }
        if (O2Levels.value > 2 && chlorophyll.value < 30 && Temperature.value > 10 && (pH.value > 4 && pH.value < 10))
        {
            bugs.SetActive(true);
        }
        else
        {
            bugs.SetActive(false);
        }
        if (O2Levels.value > 4 && chloride.value < 800 && chlorophyll.value < 30 && (pH.value > 4 && pH.value < 10) && Temperature.value > 5)
        {
            bugs2?.SetActive(true);
        }
        else
        {
            bugs2?.SetActive(false);
        }
        if (N2Levels.value >= 0 && chlorophyll.value < 30 && (pH.value > 4 && pH.value < 10))
        {
            basePlants.SetActive(true);
        }
        else
        {
            basePlants.SetActive(false);
        }
        if (N2Levels.value > 150 && chlorophyll.value < 30 && (pH.value > 4 && pH.value < 10))
        {
            nitrogenPlants.SetActive(true);
        }
        else
        {
            nitrogenPlants.SetActive(false);
        }
        if (N2Levels.value > 70 && chlorophyll.value < 30 && (pH.value > 4 && pH.value < 10))
        {
            nitrogenPlants2.SetActive(true);
        }else
        {
            nitrogenPlants2.SetActive(false);
        }
        if (Temperature.value == 0)
        {
           // water.color = new Color(0.6f, 0.82f, 0.8f, 0.3f);
        }
    }

    void PondDeath()
    {
        pondDeath.SetActive(true);
        diedOnce = true;
        O2Levels.interactable = false;
        chloride.interactable = false;
        chlorophyll.interactable = false;

        N2Levels.interactable = false;
        Temperature.interactable = false;
        pH.interactable = false;

        fertiliser.interactable = false;
        birdFeed.interactable = false;
        lawnClippings.interactable = false;
    }

    public void Rest()
    {
        fertiliser.value = 0;
        birdFeed.value = 0;
        lawnClippings.value = 0;

        DefaultIdeals();

        diedOnce = false;
        O2Levels.interactable = true;
        chloride.interactable = true;
        chlorophyll.interactable = true;

        N2Levels.interactable = true;
        Temperature.interactable = true;
        pH.interactable = true;

        fertiliser.interactable = true;
        birdFeed.interactable = true;
        lawnClippings.interactable = true;

        //N2Levels.value = 6.7f;                                          //TO DO: replace with live data
        //Temperature.value = (WD.WaterTemperature);
        //prevTemp = Temperature.value;
        //chloride.value = 5;                                             //TO DO: replace with live data
        //pH.value = WD.WaterPh;
        //chlorophyll.value = 1;                                          //TO DO: replace with live data
        //O2Levels.value = WD.DissolvedOxygen;
    }

    void LostTarget()
    {
        lostTime = Time.time;
        inFocus = false;
        TrackingManager.Instance.Tracked = false;
    }

    void GainedFocus()
    {
        //ParentCameraMatcher.Instance.SetLight();
        inFocus = true;
        TrackingManager.Instance.Tracked = true;
        FirebaseManager.Instance.ChangeScreen("Data_Game", "Station");
        if (Time.time - lostTime > 10)
        {
            playTime = 0;
            TrackingManager.Instance.GamePlayed("DataGamePlayed");
            TrackingManager.Instance.AddStation("S02");
            logged = false;
            instructions?.SetActive(true);
        }
    }
}


