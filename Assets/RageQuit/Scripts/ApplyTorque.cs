﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyTorque : MonoBehaviour
{
    Vector3 torque;
    [Range(-180, 180)]
    public float rangeMin;
    [Range(-180, 180)]
    public float rangeMax;

    void Awake()
    {
        torque.x = Random.Range(rangeMin, rangeMax);
        torque.y = Random.Range(rangeMin, rangeMax);
        torque.z = Random.Range(rangeMin, rangeMax);
        GetComponent<Rigidbody>().AddTorque(torque, ForceMode.VelocityChange);
    }
}
