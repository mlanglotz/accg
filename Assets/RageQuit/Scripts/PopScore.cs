﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopScore : MonoBehaviour
{
    Animator scoreAnim { get { return GetComponent<Animator>(); } }
    public GameObject vfx;
    public TMP_Text scoreText;
    AudioSource sfx;
    public AudioClip gameOver;
    public AudioClip gameComplete;

    private void Awake()
    {
        vfx.SetActive(false);
        sfx = GetComponent<AudioSource>();
    }

    public void ActivateScore(float score)
    {
        scoreText.text = score.ToString("f0");

        if(score <= 0)
        {
            scoreAnim.SetBool("PopScore", false);
            vfx.SetActive(false);
            CancelInvoke("ShowScore");
            return;
        }

        Invoke("ShowScore", 1);
    }

    public void ActivateGameOver()
    {
        scoreText.text = "GAME OVER";
        sfx.volume = 0.4f;
        sfx.clip = gameOver;
        sfx.Play();
        Invoke("ShowScore", 1);
    }

    void ShowScore()
    {
        scoreAnim.SetBool("PopScore", true);
        vfx.SetActive(true);
        sfx.volume = 0.75f;
        sfx.clip = gameComplete;
        sfx.Play();
    }
}
