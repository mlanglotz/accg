﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Author: Tyrone Mills - RageQuit
/// </summary>
public class XmasTreeGame : MonoBehaviour
{
    public GameObject instructions;
    [Space]
    public GameObject treeOver;

    [SerializeField] Transform candyCanes;
    [SerializeField] Transform star;
    [SerializeField] Transform ornements;
    [SerializeField] Transform presents;
    [Header("Sliders")]
    [SerializeField] Slider candyCaneSlider;
    [SerializeField] Slider starSlider;
    [SerializeField] Slider ornementsSlider;
    [SerializeField] Slider presentsSlider;

    private bool logged;
    float playTime;
    float lostTime;

    bool inFocus;

    public SoundFX GetSoundFX { get { return GetComponent<SoundFX>(); } }

    // Start is called before the first frame update
    void Start()
    {
        lostTime = Time.time;

        GetComponentInParent<DefaultTrackingEvent_RQ>().LostTarget += LostTarget;
        GetComponentInParent<DefaultTrackingEvent_RQ>().GainedTarget += GainedFocus;

        StartValues();
    }

    // Update is called once per frame
    void Update()
    {
        if (inFocus)
        {
            playTime += Time.deltaTime;
            if (Input.touchCount > 0)
            {
                Touch tTouch = Input.GetTouch(0);
                Ray tRay = Camera.main.ScreenPointToRay(tTouch.position);
                RaycastHit[] hits = Physics.RaycastAll(tRay);
                foreach (RaycastHit r in hits)
                {
                    if (r.transform.CompareTag("Interactable"))
                    {
                        r.collider.gameObject.GetComponent<RaycastReceiver>().TriggerAnim();
                    }
                }
            }
        }
        else
        {
            if (!logged && Time.time - lostTime > 5 && playTime > 5)
            {
                FirebaseManager.Instance.LogGamePlayed("XmasTree_Game", playTime);
                logged = true;
            }
        }

        if (inFocus && GetSoundFX != null)
        {

            if (!GetSoundFX.source.isPlaying)
            {
                GetSoundFX.PlaySoundLoop();
            }
        }
        else if (!inFocus && GetSoundFX != null)
        {
            if (GetSoundFX.source.isPlaying)
            {
                GetSoundFX.FadeOut();
            }
        }
    }

    public void StartValues()
    {
        treeOver.SetActive(false);

        candyCaneSlider.onValueChanged.AddListener(delegate { CandyCaneUpdate(); });
        starSlider.onValueChanged.AddListener(delegate { StarUpdate(); });
        presentsSlider.onValueChanged.AddListener(delegate { PresentsUpdate(); });
        ornementsSlider.onValueChanged.AddListener(delegate { OrnementsUpdate(); });

        candyCaneSlider.value = 2;
        starSlider.value = 0.1f;
        presentsSlider.value = 2;
        ornementsSlider.value = 2;
    }

    void CandyCaneUpdate()
    {
        for(int i = 0; i < candyCanes.childCount; i++)
        {
            if (i < candyCaneSlider.value)
            {
                candyCanes.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                candyCanes.GetChild(i).gameObject.SetActive(false);
            }
        }
        EvaluateTree();
    }

    void StarUpdate()
    {
        star.localScale = new Vector3(starSlider.value, starSlider.value, starSlider.value);
        EvaluateTree();
    }

    void PresentsUpdate()
    {
        for (int i = 0; i < presents.childCount; i++)
        {
            if (i < presentsSlider.value)
            {
                presents.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                presents.GetChild(i).gameObject.SetActive(false);
            }
        }
        EvaluateTree();
    }

    void OrnementsUpdate()
    {
        for (int i = 0; i < ornements.childCount; i++)
        {
            if (i < ornementsSlider.value)
            {
                ornements.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                ornements.GetChild(i).gameObject.SetActive(false);
            }
        }
        EvaluateTree();
    }

    void EvaluateTree()
    {
        if (candyCaneSlider.value > 6 && ornementsSlider.value > 6 && starSlider.value > (starSlider.maxValue * 0.7f))
        {
            treeOver.SetActive(true);
        }
        else
        {
            treeOver.SetActive(false);
        }
    }

    void LostTarget()
    {
        lostTime = Time.time;
        inFocus = false;
        TrackingManager.Instance.Tracked = false;
    }

    void GainedFocus()
    {
        //ParentCameraMatcher.Instance.SetLight();
        inFocus = true;
        TrackingManager.Instance.Tracked = true;
        FirebaseManager.Instance.ChangeScreen("XmasTree_Game", "Station");
        if (Time.time - lostTime > 10)
        {
            playTime = 0;
            TrackingManager.Instance.GamePlayed("XmasTree_Game_Played");
            //TrackingManager.Instance.AddStation("S02");
            logged = false;
            instructions?.SetActive(true);
        }
    }
}
