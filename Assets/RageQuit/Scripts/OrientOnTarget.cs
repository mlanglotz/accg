﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientOnTarget : MonoBehaviour
{
    public List<GameObject> targets;
    GameObject target;

    float timeSinceLastSweep;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if(timeSinceLastSweep > 3)
        {
            FindNearestTarget();
        }
        FaceTarget();
        timeSinceLastSweep += Time.deltaTime;
    }

    void FaceTarget()
    {
        if (target != null)
        {
            Debug.DrawLine(transform.position, target.transform.position, Color.red);
            Vector3 direction = target.transform.localPosition - transform.localPosition;

            direction.y = 0;
            //Debug.Log(direction);
            transform.LookAt(direction);
        }
    }

    void FindNearestTarget()
    {
        float closest = 1000;
        foreach (GameObject g in targets)
        {
            if (g.activeInHierarchy)
            {
                if (closest > Vector3.Distance(g.transform.position, transform.position))
                {
                    closest = Vector3.Distance(g.transform.position, transform.position);
                    target = g;
                }
            }
        }
        timeSinceLastSweep = 0;
    }
}
