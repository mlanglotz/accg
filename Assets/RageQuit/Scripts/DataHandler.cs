﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataHandler : Singleton<DataHandler>
{
    public WaterData WD;
    private string url = "https://canningiot-opendata.azurewebsites.net/api/OpenQuery?code=aye4fR2fxZJREaKT8ZkIWUIeQNmd73EFPer77/iKW8haDlDEIBkGGQ==&entity=WaterQuality";
  

    //https://canningiot-opendata.azurewebsites.net/api/OpenData?code=bojcyEKH8Dj4veXm5eQWSORTVFxjyGvnFa1sORW5mcwxtV43lJN85w==&entity=WaterQuality&duration=daily&format=json
    //Entity = <WaterQuality | Weather>
    //Duration = <Daily | Weekly | Monthly | All>
    //Format = <json | csv >                            defaults to csv

    public Text MobileTest;
    private void Start()
    {
        url = ScriptManager.Instance.GetText("English", "Water_Data");
        GetData();
    }

    public void GetData()
    {
        TimeSpan tSpan = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
        int tTime = PlayerPrefs.GetInt("LastWaterDataTime", 0);
        //if (tSpan.TotalMinutes - tTime > 30)
        {
            StartCoroutine(Downloader.DownloadfFomLink(url, AfterDownload));
        }
    }

    public void ForceGetData()
    {
        StartCoroutine(Downloader.DownloadfFomLink(url, AfterDownload));
    }

    public void AfterDownload(string _data)
    {
        if (_data == null)
        {
            _data = "";
            _data = PlayerPrefs.GetString("LastWaterData", _data);
            if (_data == "")
            {
                Debug.LogError("Data was null after download");
            }
            else
            {
                StartCoroutine(ProcessData(_data));
            }
        }
        else
        {
            //Debug.Log("Data" + _data);
            PlayerPrefs.SetString("LastWaterData", _data);
            TimeSpan tSpan = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            PlayerPrefs.SetInt("LastWaterDataTime", (int)tSpan.TotalMinutes);
            StartCoroutine(ProcessData(_data));
        }
    }

    IEnumerator ProcessData(string _data)
    {
        yield return null;
        //MobileTest.text = _data;
        _data = _data.Remove(0,1);
        _data = _data.Remove(_data.Length - 1);
        WD.SetToNULL();
        JsonUtility.FromJsonOverwrite(_data, WD);
        //Wait one frame to make sure processing is finished
        yield return null;
        AfterDataProcessed();
    }

    void AfterDataProcessed()
    {
        ProccessingComplete?.Invoke();
    }

    public delegate void DataDownload();
    public event DataDownload ProccessingComplete;

}
