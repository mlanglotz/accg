﻿using System.Collections;
using UnityEngine;

public class OnTurtleEat : MonoBehaviour
{
    public int myValue;
    public bool isDead = false;
    public SoundFX GetSoundFX { get { return GetComponent<SoundFX>(); } }
    //public void OnDisable()
    //{
    //    TurtleGameManager.Instance.AnimalEaten(myValue);
    //}

    public void StartActiveDelay(bool _active, float _delay)
    {
        StartCoroutine(DelayedInactive(_active, _delay));
    }


    public IEnumerator DelayedInactive(bool _active, float _delay)
    {

        yield return new WaitForSeconds(_delay);
        if (!isDead)
        {
            TurtleGameManager.Instance.AnimalEaten(myValue, transform.position);
            if (myValue > 0)
            {
                GetSoundFX.PlaySoundOneShot("CollectionSnd1");
            }
            else
            {
                GetSoundFX.PlaySoundOneShot("CollectionSnd2");
            }
        }
        isDead = true;
        foreach (Transform item in transform)
        {
            item.gameObject.SetActive(_active);
        }
      
        yield return null;
        yield return new WaitUntil(() => !GetSoundFX.source.isPlaying);
        gameObject.SetActive(_active);

    }
}
