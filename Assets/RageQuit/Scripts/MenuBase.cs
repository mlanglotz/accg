﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBase : MonoBehaviour
{
    public MenuBase previouseWindow;

    private void OnEnable()
    {
        INIT();
    }

    public void Next(MenuBase _next)
    {
        _next.SetPrev(this);
        _next.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void NextNoPrev(MenuBase _next)
    {
        _next.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }

    public void NoReturn(MenuBase _next)
    {
        _next.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
    public void NextKeep(MenuBase _next)
    {
        _next.SetPrev(this);
        _next.gameObject.SetActive(true);
    }

     void SetPrev(MenuBase _prev)
    {
        previouseWindow = _prev;
    }

    public virtual void INIT()
    {
    }

    public void Back()
    {
        if (previouseWindow != null)
        {
            previouseWindow.gameObject.SetActive(true);
            previouseWindow.INIT();
            gameObject.SetActive(false);
        }
    }

}
