﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemoteToggleActive : MonoBehaviour
{
    public Button button;
    private void Start()
    {
        button.onClick.AddListener(() => { ToggleFunction(); });
    }
    public void ToggleFunction()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);
    }
}
