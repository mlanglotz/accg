﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class TurtleGameManager : Singleton<TurtleGameManager>
{
    public MiniGameState MGS;
    public TurtleFoodMover TFM;
    public GameObject instructions;

    public GameObject infoPanel;

    public int scoreGained;
    public int winningScore;

    public Text score;
    public Text time;

    public Transform boids;
    public GameObject gameFinished;
    public TMP_Text message;
    public TMP_Text messageTitle;
    float lostTime;

    public SoundFX GetSoundFX { get { return GetComponent<SoundFX>(); } }

    [SerializeField]
    bool inFocus;
    public bool IsFocused
    {
        get { return inFocus; }
    }
    bool reset = true;

    public bool started;
    private bool logged;
    public float playTime;
    float stationDuration;

    public PopScore PS;

    // Start is called before the first frame update
    void Start()
    {
        lostTime = Time.time;
        score.text = "0";
        score.transform.parent.gameObject.SetActive(false);
        MGS.score = 0;
        GetComponentInParent<DefaultTrackingEvent_RQ>().LostTarget += LostTarget;
        GetComponentInParent<DefaultTrackingEvent_RQ>().GainedTarget += GainedFocus;
    }

    private void Update()
    {
#if !UNITY_EDITOR
        if (!inFocus && !reset)
        {
            if (Time.time - lostTime > 10)
            {
                ResetGame();
            }
        }
#endif
        if (inFocus)
        {
            stationDuration += Time.deltaTime;
                score.transform.parent.gameObject.SetActive(!instructions.activeInHierarchy);
            if (!instructions.activeInHierarchy && !gameFinished.activeInHierarchy)
            {
                playTime += Time.deltaTime;
                    time.text = TimeSpan.FromSeconds(60-playTime).ToString("mm\\:ss");
                if (playTime > 60 && !gameFinished.activeInHierarchy)
                {
                    gameFinished.SetActive(true);
                    //messageTitle.text = "Well Done";
                    messageTitle.GetComponent<TextOverrider>().SetKeyandUpdate("S05_Win");
                    message.text = $"Congratulations your score was: {MGS.score}";
                    PS.ActivateScore(MGS.score);
                    score.transform.parent.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            if (!logged && Time.time - lostTime > 5 && stationDuration > 5)
            {
                FirebaseManager.Instance.LogGamePlayed("Turtle_Game", stationDuration);
                logged = true;
            }
        }

        if (inFocus && GetSoundFX != null)
        {

            if (!GetSoundFX.source.isPlaying)
            {
                GetSoundFX.PlaySoundLoop();
            }
        }
        else if (!inFocus && GetSoundFX != null)
        {
            if (GetSoundFX.source.isPlaying)
            {
                GetSoundFX.FadeOut();
            }
        }
    }

    public void AnimalEaten(int _score, Vector3 _pos)
    {
        ScreenEffects.Instance.ScoreEffect(_score, _pos);
        MGS.score += _score;
        scoreGained += _score;
        score.text = MGS.score.ToString();
        if (MGS.score < 0)
        {
            gameFinished.SetActive(true);
            PS.ActivateGameOver();
            score.transform.parent.gameObject.SetActive(false);
            messageTitle.text = ScriptManager.Instance.GetText("English", "S05_Try_Again_Title");
            message.text = "Remember to avoid eating human food.";
        }
        reset = false;
    }


    void LostTarget()
    {
        lostTime = Time.time;
        inFocus = false;
        MGS.timeScale = 0;
        TrackingManager.Instance.Tracked = false;
        reset = false;
    }

    void GainedFocus()
    {
        //ParentCameraMatcher.Instance.SetLight();
        inFocus = true;
        MGS.timeScale = 1;
        TrackingManager.Instance.Tracked = true;
        FirebaseManager.Instance.ChangeScreen("Turtle_Game", "Station");
        if (Time.time - lostTime > 10)
        {
            stationDuration = 0;
            TrackingManager.Instance.GamePlayed("TurtleGamePlayed");
            ResetGame();
            instructions?.SetActive(true);
            score.transform.parent.gameObject.SetActive(false);
            TrackingManager.Instance.AddStation("S05");
        }
    }

    public void ResetGame()
    {
        MGS.score = 0;
        playTime = 0;
        TFM.speed = 10;
        TFM.addRate = 2;
        score.text = MGS.score.ToString();
        gameFinished.SetActive(false);
        reset = true;
        PS.ActivateScore(0);
    }

    public void StartGame()
    {
        started = true;
    }
}
