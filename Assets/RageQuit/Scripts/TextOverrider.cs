﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

//[RequireComponent(typeof(Text))]
public class TextOverrider : MonoBehaviour, IPointerClickHandler
{
    [Tooltip("Key to use to search for data.")]
    public string key;
    [Tooltip("What behaviour should be used on the text element.")]
    public TextBehaviour behaviour;

    Text text { get { return GetComponent<Text>(); } }
    TMP_Text textTMP { get { return GetComponent<TMP_Text>(); } }
    string message;
    // Start is called before the first frame update
    void Start()
    {
        if (ScriptManager.Instance != null)
        {
            if (ScriptManager.loaded)
            {
                InitialiseText();
            }
            else
            {
                ScriptManager.Instance.hasLoaded += InitialiseText;
            }
        }
        else
        {
            Debug.LogWarning("No Script Manager Found in scene");
        }
    }

    void InitialiseText()
    {
        //ScriptManager.Instance.TestPrint();
        //return;
        GetMessage();
        switch (behaviour)
        {
            case TextBehaviour.Prepend:
                {
                    if (text != null) { SetText(message + text.text); } // Send prepended message to all text fields
                    if (textTMP != null) { SetText(message + textTMP.text); }
                }
                break;
            case TextBehaviour.OverwriteLocal:
                {
                    
                }
                break;
            case TextBehaviour.Append:
                {
                    if (text != null){ SetText(text.text + message); } // Send appended message to all text fields
                    if (textTMP != null) { SetText(textTMP.text + message); }
                }
                break;
            case TextBehaviour.OverwriteServer:
                {
                    SetText(message); // Send message to all text fields
                }
                break;
        }
        ScriptManager.Instance.hasLoaded -= InitialiseText;
    }

    void GetMessage()
    {
        if(key == "NONE") { return; }
        message = ScriptManager.Instance.GetText("English", key);
    }

    public void SetKeyandUpdate(string _key)
    {
        key = _key;

        InitialiseText();
    }

    /// <summary>
    /// Detect which type of text is being used and apply message
    /// </summary>
    /// <param name="s"></param>
    /// <param name="tmp"></param>
    /// <param name="txt"></param>
    void SetText (string s)
    {
        if (text != null) { text.text = s; }
        if (textTMP != null) { textTMP.text = s; }
    }

    /// <summary>
    /// Author: Tyrone Mills - RageQuit
    /// Descrtiption: Respond to user clicking on text that we have set a link value for
    /// </summary>
    /// <param name="_event"></param>
    public void OnPointerClick(PointerEventData _event)
    {
        if (textTMP != null)
        {
            if (_event.button == PointerEventData.InputButton.Left)
            {
                int tLinkIndex = TMP_TextUtilities.FindIntersectingLink(textTMP, Input.mousePosition, null);
                if (tLinkIndex > -1)
                {
                    var tLinkInfo = textTMP.textInfo.linkInfo[tLinkIndex];
                    var tLinkId = tLinkInfo.GetLinkID();

                    OpenURLLinkFromText(tLinkId);
                }
            }
        }
    }

    /// <summary>
    /// Author: Tyrone Mills - RageQuit
    /// Description: Open a URL link from text link
    /// </summary>
    /// <param name="_url"></param>
    void OpenURLLinkFromText(string _url)
    {
        Application.OpenURL(_url);
    }
}

public enum TextBehaviour
{
    Prepend,
    OverwriteLocal,
    Append,
    OverwriteServer
}