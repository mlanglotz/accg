﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScreenEffects : Singleton<ScreenEffects>
{
    public Animator RedFlash;
    public Animator GreenFlash;
    public GameObject PopScore;

    public Color Negative;
    public Color Positive;

    public void ScoreEffect(float score, Vector3 _pos)
    {
        var go = Instantiate(PopScore);
        go.transform.SetParent(transform);
        go.GetComponentInChildren<TMP_Text>().text = score.ToString();
        go.GetComponent<Animator>().SetTrigger("Activate");
        go.GetComponent<UITrackPosition>().TrackingTarget = _pos;

        if (score > 0)
        {
            go.GetComponentInChildren<TMP_Text>().color = Positive;
            GreenFlash.SetTrigger("Activate");
        }
        else
        {
            go.GetComponentInChildren<TMP_Text>().color = Negative;
            RedFlash.SetTrigger("Activate");
        }
    }
}
