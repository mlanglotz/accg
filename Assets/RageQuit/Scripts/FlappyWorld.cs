﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FlappyWorld : MonoBehaviour
{
    // Start is called before the first frame update
    public FlappyBirdLogic Logic; //The Bat
    public Transform ObstaclesTransform; //The Obstacles in scene
    public Transform CollectablesTransform; //Collectables in Scene
    public Transform GroundTransform; //The Land objects in scene
    public List<GameObject> ObstacleObjects; //Obstacle Gameobjects for instatiate
    public GameObject land; //Land Object for Instatiate
    public float deleteRange; //Where land gets deleted
    public Vector3 StartPosition; // Bat start position
    public MiniGameState MGS; //mini game state
    public float CollectableSpawnChance; //0 - 100 chance a collectable will spawn on top of obstacle
    public List<GameObject> CollectableObjects; //All the objects that can be spawned
    public List<Vector3> ObstacleStartPositions; //start position for default obstacle spawn
    public List<Vector3> GroundStartPositions; //start position for default land spawn
    public GameObject InstructionsPanel;
    public GameObject InstructionsPanel2;
    public BatGameManager BGM { get { return BatGameManager.Instance; } }
    public SoundFX GetSoundFX { get { return GetComponent<SoundFX>(); } }
    bool ready = false; //ready to start game
    float s = 0;
    private void Start()
    {
        Logic.dead = true;
        Logic.GetRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        Logic.GetRigidbody.angularVelocity = Vector3.zero;
        Logic.GetRigidbody.velocity = Vector3.zero;
        Logic.CollectableHit += OnCollectable;
        Logic.Dead += onDead;
        StartPosition = Logic.transform.position;
        ObstacleStartPositions = new List<Vector3>();
        GroundStartPositions = new List<Vector3>();
        foreach (Transform item in ObstaclesTransform)
        {
            ObstacleStartPositions.Add(item.localPosition);
        }
        foreach (Transform item in GroundTransform)
        {
            GroundStartPositions.Add(item.localPosition);
        }
        ready = true;
        Logic.gestures.Tapper += () =>
        {
            if (ready && Logic.dead && BatGameManager.Instance.IsFocused && !InstructionsPanel.activeInHierarchy)
            {
                Logic.dead = false;
                Logic.GetRigidbody.constraints = RigidbodyConstraints.None;
                Logic.GetRigidbody.angularVelocity = Vector3.zero;
                Logic.GetRigidbody.velocity = Vector3.zero;
                //InstructionsPanel2.SetActive(false);
                BGM.TotalScore = 0;
            }
        };
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            StartGame();
        }
        if (!Logic.dead && BatGameManager.Instance.IsFocused && !BatGameManager.Instance.infoPanel.activeInHierarchy && !BatGameManager.Instance.instructions.activeInHierarchy)
        {
            Move();
            BGM.TotalScore += Mathf.Abs(Time.deltaTime * Logic.Speed * MGS.timeScale);
        }
        else
        {
            Logic.GetRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }
        if (BatGameManager.Instance.IsFocused && GetSoundFX != null)
        {

            if (!GetSoundFX.source.isPlaying)
            {
                GetSoundFX.PlaySoundLoop();
            }
        }
        else if(!BatGameManager.Instance.IsFocused && GetSoundFX != null)
        {
            if (GetSoundFX.source.isPlaying)
            {
                GetSoundFX.FadeOut();
            }
        }

    }

    /// <summary>
    /// Starts the game
    /// </summary>
    public void StartGame()
    {
        BGM.ResetGame();
        ready = false;
        StartCoroutine(ActuallyStart());
    }

    /// <summary>
    /// Because I need a frame;
    /// </summary>
    /// <returns> wait for 1 second</returns>
    IEnumerator ActuallyStart()
    {
        Logic.gameObject.SetActive(true);
        Logic.transform.position = StartPosition;
        Logic.dead = true;
        Logic.GetRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        Logic.GetRigidbody.angularVelocity = Vector3.zero;
        Logic.GetRigidbody.velocity = Vector3.zero;
        BGM.TotalScore = 0;
        Logic.Speed = Logic.startingSpeed;
        foreach (Transform item in ObstaclesTransform)
        {
            Destroy(item.gameObject);

        }
        foreach (Transform item in GroundTransform)
        {
            Destroy(item.gameObject);
        }
        foreach (Transform item in CollectablesTransform)
        {
            Destroy(item.gameObject);
        }

        for (int i = 0; i < 5; i++)
        {
            var pos = ObstacleStartPositions[i];

            var rand = Random.Range(0 , ObstacleObjects.Count);
            for (int x = 0; x < 10; x++)
            {
                rand = Random.Range(0 , ObstacleObjects.Count);
                if (ObstacleObjects[rand].GetComponent<Obstacle>().CanSpawnStart)
                {
                    var obs = Instantiate(ObstacleObjects[rand] , ObstaclesTransform);
                    var newpos = obs.transform.localPosition;
                    newpos.x = pos.x;
                    obs.transform.localPosition = newpos;
                    break;
                }
            }

            pos = GroundStartPositions[i];
            var l = Instantiate(land , GroundTransform);
            l.transform.localPosition = pos;
        }
        yield return new WaitForSeconds(1f);
        Logic.dead = false;
        Logic.GetRigidbody.constraints = RigidbodyConstraints.None;
        Logic.GetRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        ready = true;
    }

    /// <summary>
    /// Move all Obstacles and Land Pieces
    /// </summary>
    public void Move()
    {
        foreach (Transform item in ObstaclesTransform)
        {
            var pos = item.localPosition;
            pos.x += Logic.Speed * Time.deltaTime * MGS.timeScale;
            item.localPosition = pos;

        }
        foreach (Transform item in GroundTransform)
        {
            var pos = item.localPosition;
            pos.x += Logic.Speed * Time.deltaTime * MGS.timeScale;
            item.localPosition = pos;
        }
        foreach (Transform item in CollectablesTransform)
        {
            var pos = item.localPosition;
            pos.x += Logic.Speed * Time.deltaTime * MGS.timeScale;
            item.localPosition = pos;
        }

        if (ObstaclesTransform.GetChild(0).localPosition.x < deleteRange)
        {
            Debug.Log("Spawned : " + ObstaclesTransform.GetChild(0).GetInstanceID());
            Spawn();
            if (ObstaclesTransform.GetChild(0).GetComponent<Obstacle>().onMe != null)
            {
                Destroy(ObstaclesTransform.GetChild(0).GetComponent<Obstacle>().onMe.gameObject);
            }
            Destroy(ObstaclesTransform.GetChild(0).gameObject);
            Destroy(GroundTransform.GetChild(0).gameObject);

        }


    }

    /// <summary>
    /// Spawn a new Land piece and Obstacle
    /// </summary>
    public void Spawn()
    {
        s++;
        var pos = ObstaclesTransform.GetChild(ObstaclesTransform.childCount - 1).localPosition;
        pos.x += 35;
        var rand = Random.Range(0 , ObstacleObjects.Count);

        var obs = Instantiate(ObstacleObjects[rand] , ObstaclesTransform);
        var newpos = obs.transform.localPosition;
        newpos.x = pos.x;
        obs.transform.localPosition = newpos;

        rand = Random.Range(0 , 100);
        if (rand < CollectableSpawnChance)
        {
            for (int i = 0; i < 20; i++)
            {
                var r = Random.Range(0 , CollectableObjects.Count);
                if (obs.GetComponent<Obstacle>().height == SpawnHeight.Low && CollectableObjects[r].GetComponent<FlappyCollectable>().height != SpawnHeight.Low) //Low obstacle = med -> high collectable only
                {
                    var col = Instantiate(CollectableObjects[r] , CollectablesTransform);
                    var p = col.transform.localPosition;
                    p.x = pos.x;
                    col.transform.localPosition = p;
                    obs.GetComponent<Obstacle>().onMe = col.GetComponent<FlappyCollectable>();
                    break;
                }
                else if (obs.GetComponent<Obstacle>().height == SpawnHeight.Medium && CollectableObjects[r].GetComponent<FlappyCollectable>().height != SpawnHeight.Medium) //med obstacle = low || high collectable only
                {
                    var col = Instantiate(CollectableObjects[r] , CollectablesTransform);
                    var p = col.transform.localPosition;
                    p.x = pos.x;
                    col.transform.localPosition = p;
                    obs.GetComponent<Obstacle>().onMe = col.GetComponent<FlappyCollectable>();
                    break;
                }
                else if (obs.GetComponent<Obstacle>().height == SpawnHeight.High && CollectableObjects[r].GetComponent<FlappyCollectable>().height != SpawnHeight.High) //high obstacle = med -> low collectable only
                {
                    var col = Instantiate(CollectableObjects[r] , CollectablesTransform);
                    var p = col.transform.localPosition;
                    p.x = pos.x;
                    col.transform.localPosition = p;
                    obs.GetComponent<Obstacle>().onMe = col.GetComponent<FlappyCollectable>();
                    break;
                }

            }
        }

        pos = GroundTransform.GetChild(GroundTransform.childCount - 1).localPosition;
        pos.x += 35f;
        var l = Instantiate(land , GroundTransform);
        l.transform.localPosition = pos;

    }

    public void OnCollectable(FlappyCollectable collectable)
    {
        BGM.TotalScore += collectable.Points;
        ScreenEffects.Instance.ScoreEffect(collectable.Points, collectable.transform.position);
        Destroy(collectable.gameObject);
    }

    /// <summary>
    /// Called when bat dies
    /// </summary>
    public void onDead()
    {
        ready = false;
    }
}

public enum SpawnHeight
{
    Low,
    Medium,
    High
}