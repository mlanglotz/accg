﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class SoundFX : MonoBehaviour
{
    public string defaultSound;
    public bool FadeInDefault;
    [SerializeField]
    public static float FadeSpeed = 1;
    public SoundLibrary library;
    public List<Coroutine> coroutines;
    public AudioSource source { get { return GetComponent<AudioSource>(); } }
    bool FadingIn = false;
    bool FadingOut = false;
    float fadeT = 0f;
    float currVol;
    
    // Start is called before the first frame update
    private void Start()
    {
        coroutines = new List<Coroutine>();
        if (FadeInDefault)
        {
            FadeIn();
            PlaySoundLoop(defaultSound);
        }
    }

    private void Update()
    {
        if (FadingIn)
        {
            fadeT += Time.deltaTime * FadeSpeed;
            source.volume = Mathf.Lerp(currVol , 1 , fadeT);
            if (fadeT >= 1)
            {
                FadingIn = false;
                FadingOut = false;
                currVol = source.volume;
            }
        }
        else if (FadingOut)
        {
            fadeT += Time.deltaTime * FadeSpeed;
            source.volume = Mathf.Lerp(0 , currVol , fadeT);
            if (fadeT >= 1)
            {
                FadingIn = false;
                FadingOut = false;
                currVol = source.volume;
                StopAllSounds();
            }
        }
    }

    public void FadeIn()
    {
        FadingOut = false;
        FadingIn = true;
        source.volume = 0;
        currVol = source.volume;
        fadeT = 0;
    }

    public void FadeOut()
    {
        FadingIn = false;
        FadingOut = true;
        currVol = source.volume;
    }

    /// <summary>
    /// Play a sound
    /// </summary>
    /// <param name="sound">The sound to play</param>
    public void PlaySoundOneShot(string sound)
    {
        source.PlayOneShot(library.GetSound(sound));
    }

    /// <summary>
    /// Play the default sound
    /// </summary>
    public void PlaySoundOneShot()
    {
        if (!string.IsNullOrEmpty(defaultSound))
        {
            source.PlayOneShot(library.GetSound(defaultSound));
        }
    }

    /// <summary>
    /// Play a sound and loop it
    /// </summary>
    /// <param name="sound">The sound to play</param>
    public void PlaySoundLoop(string sound)
    {
        source.clip = library.GetSound(sound);
        source.loop = true;
        source.Play();
    }

    /// <summary>
    /// Play default Sound and loop it
    /// </summary>
    public void PlaySoundLoop()
    {
        FadeIn();
        source.clip = library.GetSound(defaultSound);
        source.loop = true;
        source.Play();
    }

    /// <summary>
    /// Play a sound oneshot at a custom delay
    /// </summary>
    /// <param name="delay">the delay</param>
    /// <param name="sound">the sound to play</param>
    public void PlayRepeatingSound(float delay , string sound)
    {
        coroutines.Add(StartCoroutine(RepeatSound(delay , sound)));
    }

    /// <summary>
    /// Execute the oneshot at the custom delay
    /// </summary>
    /// <param name="delay">the Delay</param>
    /// <param name="sound">the sound to play</param>
    /// <returns> yield for (delay) seconds </returns>
    IEnumerator RepeatSound(float delay , string sound)
    {
        while (true)
        {
            source.PlayOneShot(library.GetSound(sound));
            yield return new WaitForSeconds(delay);
        }
    }

    /// <summary>
    /// Stop every sound
    /// </summary>
    public void StopAllSounds()
    {
        source.Stop();
        source.clip = null;
        if (coroutines.Count > 0)
        {
            foreach (var item in coroutines)
            {
                StopCoroutine(item);
            }
        }
    }
}
