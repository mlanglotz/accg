﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weather Data", menuName = "RageQuit/Weather Data", order = 11)]
public class WeatherData : ScriptableObject
{
    public string id;
    public string PartitionKey;
    public string DeviceID;
    public string ObservationTimestamp;
    public float BatteryLevel;
    public float DissolvedOxygen;
    public float RelativeHumidity;
    public float BarometricPressure;
    public float AirTemperature;
    public string _DataValue;
    public string _TimeValue;
    public string _rid;
    public string _self;
    public string _etag;
    public string _attachments;
    public string _ts;
}
